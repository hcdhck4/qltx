﻿using System;
using System.Windows.Forms;
using DTO;
using BUS;
using DevExpress.XtraEditors;
using System.Data;
using DevExpress.XtraEditors.Controls;

namespace QLDuongSat.UC_He_Thong
{
    public partial class SuaTaiKhoan : DevExpress.XtraEditors.XtraForm
    {
        public TaiKhoanDTO TaiKhoan = new TaiKhoanDTO();
        
        public SuaTaiKhoan()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            //Kiểm tra 
            if (Txtmatk.Text == "")
            {
                dxErrorProvider1.SetError(Txtmatk, "Vui lòng nhập thông tin!");
                MessageBox.Show("Mã tài khoản không được để trống!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (TxtTenTk.Text == "")
            {
                dxErrorProvider1.SetError(TxtTenTk, "Vui lòng nhập thông tin!");
                MessageBox.Show("Tên tài khoản không được để trống!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (TxtMatKhau.Text == "")
            {
                dxErrorProvider1.SetError(TxtMatKhau, "Vui lòng nhập thông tin!");
                MessageBox.Show("Mật khẩu không được để trống!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (CbboxVaiTro.Text == "")
            {
                dxErrorProvider1.SetError(CbboxVaiTro, "Vui lòng nhập thông tin!");
                MessageBox.Show("Vai trò không được để trống!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (GridLKNhanVien.Text == "")
            {
                dxErrorProvider1.SetError(GridLKNhanVien, "Vui lòng nhập thông tin!");
                MessageBox.Show("Nhân viên không được để trống!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            TaiKhoanDTO taikhoan = new TaiKhoanDTO();
            taikhoan.Matk = Txtmatk.Text;
            taikhoan.Tentk = TxtTenTk.Text;
            taikhoan.Matkhau = TxtMatKhau.Text;
            if (CheckTrangThai.Enabled == true)
            {
                taikhoan.Tinhtrang = true;
            }
            else
            {
                taikhoan.Tinhtrang = false;
            }
            taikhoan.Ghichu = TxtGhiChu.Text;
            taikhoan.Manv = gridLookUpEdit1View.GetFocusedRowCellValue(gridColumn1).ToString();
            taikhoan.Vaitro = CbboxVaiTro.Text;
            try
            {
                VaiTroBus.SuaTK(taikhoan);
                MessageBox.Show("Cập nhật thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Kiểm tra lại mã tài khoản", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Txtmatk_TextChanged(object sender, EventArgs e)
        {
            if (Txtmatk.Text == "")
            {
                dxErrorProvider1.SetError(Txtmatk, "Vui lòng nhập thông tin!");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void TxtTenTk_TextChanged(object sender, EventArgs e)
        {
            if (TxtTenTk.Text == "")
            {
                dxErrorProvider1.SetError(TxtTenTk, "Vui lòng nhập thông tin!");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void TxtMatKhau_TextChanged(object sender, EventArgs e)
        {
            if (TxtMatKhau.Text == "")
            {
                dxErrorProvider1.SetError(TxtMatKhau, "Vui lòng nhập thông tin!");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void CbboxVaiTro_TextChanged(object sender, EventArgs e)
        {
            if (CbboxVaiTro.Text == "")
            {
                dxErrorProvider1.SetError(CbboxVaiTro, "Vui lòng nhập thông tin!");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void GridLKNhanVien_TextChanged(object sender, EventArgs e)
        {
            if (GridLKNhanVien.Text == "")
            {
                dxErrorProvider1.SetError(GridLKNhanVien, "Vui lòng nhập thông tin!");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void SuaTaiKhoan_Load(object sender, EventArgs e)
        {
            Txtmatk.Enabled = false;
            Txtmatk.Text = TaiKhoan.Matk;
            TxtTenTk.Text = TaiKhoan.Tentk;
            if (TaiKhoan.Tinhtrang == true)
            {
                CheckTrangThai.Checked = true;
            }
            else
            { CheckTrangThai.Checked = false ; }

            ComboBoxEdit combo = new ComboBoxEdit();
            ComboBoxItemCollection dong = CbboxVaiTro.Properties.Items;
            dong.BeginUpdate();
            dong.Add("Admin");
            dong.Add("Nhân Viên Tin Học");
            dong.Add("Quản Lý");
            dong.Add("Nhân Viên Bán Vé");
            dong.EndUpdate();
            combo.SelectedIndex = -1;
            CbboxVaiTro.Text = TaiKhoan.Vaitro;
            ///
            DataTable bangnv = new DataTable();
            bangnv = NhanVienBUS.LayTenMaNhanvien();
            GridLKNhanVien.Properties.DataSource = bangnv;
            GridLKNhanVien.Properties.DisplayMember = "Tennv";
            GridLKNhanVien.Properties.ValueMember = "Manv";
            GridLKNhanVien.Text = TaiKhoan.Manv ;
        }

        private void btThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}