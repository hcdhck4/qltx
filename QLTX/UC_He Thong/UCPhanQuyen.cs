﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BUS;
using DTO;

namespace QLDuongSat.UC_He_Thong
{
    public partial class UCPhanQuyen : UserControl
    {
        public UCPhanQuyen()
        {
            InitializeComponent();
        }
        public string matk;
        public TaiKhoanDTO TaiKhoan = new TaiKhoanDTO();
        private void UCPhanQuyen_Load(object sender, EventArgs e)
        {
            DataTable bang = new DataTable();
            bang = VaiTroBus.LayBangTaiKhoan();
            GridTaiKhoan.DataSource = bang;
            gridView2.Columns["Matk"].Visible = false;
        }
        public void LoadDS()
        {
            DataTable bang = new DataTable();
            bang = VaiTroBus.LayBangTaiKhoan();
            GridTaiKhoan.DataSource = bang;
            gridView2.Columns["Matk"].Visible = false;
        }
        //
        public void GetValue()
        {
            LoadDS();
        }
        //
        private void BttThem_Click(object sender, EventArgs e)
        {
            ThemTK themtk = new ThemTK();
            themtk.MyGetData = new ThemTK.GetString(GetValue);
            themtk.ShowDialog();
        }

        private void BttNapLai_Click(object sender, EventArgs e)
        {
            UCPhanQuyen_Load(sender, e);
        }

        private void BttIn_Click(object sender, EventArgs e)
        {
            GridTaiKhoan.ShowPrintPreview();
        }

        private void GridTaiKhoan_Click(object sender, EventArgs e)
        {
            matk = gridView2.GetFocusedRowCellDisplayText(gridColumn6);
            //
            TaiKhoan.Matk = gridView2.GetFocusedRowCellValue(gridColumn6).ToString();
            TaiKhoan.Tentk = gridView2.GetFocusedRowCellValue(gridColumn3).ToString();
            TaiKhoan.Manv  = gridView2.GetFocusedRowCellValue(gridColumn4).ToString();
            TaiKhoan.Vaitro  = gridView2.GetFocusedRowCellValue(gridColumn2).ToString();
            TaiKhoan.Tinhtrang  = (Boolean)gridView2.GetFocusedRowCellValue(gridColumn5);
            
        }

        private void BttXoa_Click(object sender, EventArgs e)
        {
          DialogResult  result  = MessageBox.Show("Bạn có muốn xóa?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes) 
        {
            VaiTroBus.Xoataikhoan(matk);
            MessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK , MessageBoxIcon.Information );
            UCPhanQuyen_Load(sender, e);
        }        
        }

        private void BttSua_Click(object sender, EventArgs e)
        {
            SuaTaiKhoan frmsua = new SuaTaiKhoan();
            frmsua.TaiKhoan = TaiKhoan;
            frmsua.ShowDialog();
        }
       
    }
}
