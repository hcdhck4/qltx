﻿namespace QLDuongSat.UC_He_Thong
{
    partial class ThemTK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThemTK));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.Txtmatk = new DevExpress.XtraEditors.TextEdit();
            this.btThoat = new DevExpress.XtraEditors.SimpleButton();
            this.TxtTenTk = new DevExpress.XtraEditors.TextEdit();
            this.btLuu = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.TxtGhiChu = new System.Windows.Forms.TextBox();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.TxtMatKhau = new DevExpress.XtraEditors.TextEdit();
            this.CbboxVaiTro = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CheckTrangThai = new DevExpress.XtraEditors.CheckEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridLKNhanVien = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Txtmatk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenTk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMatKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbboxVaiTro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckTrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLKNhanVien.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.GridLKNhanVien);
            this.panelControl1.Controls.Add(this.CheckTrangThai);
            this.panelControl1.Controls.Add(this.CbboxVaiTro);
            this.panelControl1.Controls.Add(this.TxtMatKhau);
            this.panelControl1.Controls.Add(this.Txtmatk);
            this.panelControl1.Controls.Add(this.btThoat);
            this.panelControl1.Controls.Add(this.TxtTenTk);
            this.panelControl1.Controls.Add(this.btLuu);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.TxtGhiChu);
            this.panelControl1.Location = new System.Drawing.Point(12, 44);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(305, 365);
            this.panelControl1.TabIndex = 17;
            // 
            // Txtmatk
            // 
            this.Txtmatk.Location = new System.Drawing.Point(106, 18);
            this.Txtmatk.Name = "Txtmatk";
            this.Txtmatk.Size = new System.Drawing.Size(177, 20);
            this.Txtmatk.TabIndex = 1;
            this.Txtmatk.TextChanged += new System.EventHandler(this.Txtmatk_TextChanged);
            // 
            // btThoat
            // 
            this.btThoat.Image = ((System.Drawing.Image)(resources.GetObject("btThoat.Image")));
            this.btThoat.Location = new System.Drawing.Point(174, 299);
            this.btThoat.Name = "btThoat";
            this.btThoat.Size = new System.Drawing.Size(75, 44);
            this.btThoat.TabIndex = 8;
            this.btThoat.Text = "Thoát";
            this.btThoat.Click += new System.EventHandler(this.btThoat_Click);
            // 
            // TxtTenTk
            // 
            this.TxtTenTk.Location = new System.Drawing.Point(106, 62);
            this.TxtTenTk.Name = "TxtTenTk";
            this.TxtTenTk.Size = new System.Drawing.Size(177, 20);
            this.TxtTenTk.TabIndex = 3;
            this.TxtTenTk.TextChanged += new System.EventHandler(this.TxtTenTk_TextChanged);
            // 
            // btLuu
            // 
            this.btLuu.Image = ((System.Drawing.Image)(resources.GetObject("btLuu.Image")));
            this.btLuu.Location = new System.Drawing.Point(76, 299);
            this.btLuu.Name = "btLuu";
            this.btLuu.Size = new System.Drawing.Size(82, 44);
            this.btLuu.TabIndex = 7;
            this.btLuu.Text = "Lưu";
            this.btLuu.Click += new System.EventHandler(this.btLuu_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl2.Location = new System.Drawing.Point(16, 66);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Tên tài khoản";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl1.Location = new System.Drawing.Point(16, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(73, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mã tài khoản";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl3.Location = new System.Drawing.Point(16, 223);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 16);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Ghi Chú";
            // 
            // TxtGhiChu
            // 
            this.TxtGhiChu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TxtGhiChu.Location = new System.Drawing.Point(106, 223);
            this.TxtGhiChu.Multiline = true;
            this.TxtGhiChu.Name = "TxtGhiChu";
            this.TxtGhiChu.Size = new System.Drawing.Size(177, 62);
            this.TxtGhiChu.TabIndex = 6;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Location = new System.Drawing.Point(12, 12);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(305, 26);
            this.panelControl2.TabIndex = 16;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl4.Location = new System.Drawing.Point(60, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(189, 16);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "THÊM TÀI KHOẢN NGƯỜI DÙNG";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl5.Location = new System.Drawing.Point(16, 102);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 16);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Mật khẩu";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl6.Location = new System.Drawing.Point(15, 138);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(38, 16);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Vai trò";
            // 
            // TxtMatKhau
            // 
            this.TxtMatKhau.Location = new System.Drawing.Point(106, 102);
            this.TxtMatKhau.Name = "TxtMatKhau";
            this.TxtMatKhau.Properties.PasswordChar = '*';
            this.TxtMatKhau.Size = new System.Drawing.Size(177, 20);
            this.TxtMatKhau.TabIndex = 9;
            this.TxtMatKhau.TextChanged += new System.EventHandler(this.TxtMatKhau_TextChanged);
            // 
            // CbboxVaiTro
            // 
            this.CbboxVaiTro.Location = new System.Drawing.Point(105, 138);
            this.CbboxVaiTro.Name = "CbboxVaiTro";
            this.CbboxVaiTro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbboxVaiTro.Size = new System.Drawing.Size(178, 20);
            this.CbboxVaiTro.TabIndex = 10;
            this.CbboxVaiTro.TextChanged += new System.EventHandler(this.CbboxVaiTro_TextChanged);
            // 
            // CheckTrangThai
            // 
            this.CheckTrangThai.Location = new System.Drawing.Point(206, 202);
            this.CheckTrangThai.Name = "CheckTrangThai";
            this.CheckTrangThai.Properties.Caption = "Trạng thái";
            this.CheckTrangThai.Size = new System.Drawing.Size(75, 19);
            this.CheckTrangThai.TabIndex = 11;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl7.Location = new System.Drawing.Point(16, 171);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(56, 16);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Nhân viên";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // GridLKNhanVien
            // 
            this.GridLKNhanVien.Location = new System.Drawing.Point(105, 170);
            this.GridLKNhanVien.Name = "GridLKNhanVien";
            this.GridLKNhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GridLKNhanVien.Properties.NullText = "";
            this.GridLKNhanVien.Properties.View = this.gridLookUpEdit1View;
            this.GridLKNhanVien.Size = new System.Drawing.Size(178, 20);
            this.GridLKNhanVien.TabIndex = 12;
            this.GridLKNhanVien.TextChanged += new System.EventHandler(this.GridLKNhanVien_TextChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã nhân viên";
            this.gridColumn1.FieldName = "Manv";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên nhân viên";
            this.gridColumn2.FieldName = "Tennv";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // ThemTK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 410);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "ThemTK";
            this.Text = "Thêm tài khoản";
            this.Load += new System.EventHandler(this.ThemTK_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Txtmatk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenTk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMatKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbboxVaiTro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckTrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLKNhanVien.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit CheckTrangThai;
        private DevExpress.XtraEditors.ComboBoxEdit CbboxVaiTro;
        private DevExpress.XtraEditors.TextEdit TxtMatKhau;
        private DevExpress.XtraEditors.TextEdit Txtmatk;
        private DevExpress.XtraEditors.SimpleButton btThoat;
        private DevExpress.XtraEditors.TextEdit TxtTenTk;
        private DevExpress.XtraEditors.SimpleButton btLuu;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.TextBox TxtGhiChu;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.GridLookUpEdit GridLKNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}