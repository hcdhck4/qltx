﻿namespace QLDuongSat.UC_He_Thong
{
    partial class UCPhanQuyen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCPhanQuyen));
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.GridTaiKhoan = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BttXuat = new DevExpress.XtraEditors.SimpleButton();
            this.BttThem = new DevExpress.XtraEditors.SimpleButton();
            this.BttIn = new DevExpress.XtraEditors.SimpleButton();
            this.BttXoa = new DevExpress.XtraEditors.SimpleButton();
            this.BttNapLai = new DevExpress.XtraEditors.SimpleButton();
            this.BttSua = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridTaiKhoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl2.Location = new System.Drawing.Point(47, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(256, 31);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Thông Tin Tài Khoản";
            // 
            // GridTaiKhoan
            // 
            this.GridTaiKhoan.Location = new System.Drawing.Point(0, 129);
            this.GridTaiKhoan.MainView = this.gridView2;
            this.GridTaiKhoan.Name = "GridTaiKhoan";
            this.GridTaiKhoan.Size = new System.Drawing.Size(1012, 358);
            this.GridTaiKhoan.TabIndex = 2;
            this.GridTaiKhoan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.GridTaiKhoan.Click += new System.EventHandler(this.GridTaiKhoan_Click);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.GridControl = this.GridTaiKhoan;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Vai Trò";
            this.gridColumn2.FieldName = "VaiTro";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Tên tài khoản";
            this.gridColumn3.FieldName = "Tennguoidung";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Mô Tả";
            this.gridColumn4.FieldName = "Tennv";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Tình Trạng";
            this.gridColumn5.FieldName = "Tinhtrang";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Mã Tài Khoản";
            this.gridColumn6.FieldName = "Matk";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // BttXuat
            // 
            this.BttXuat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttXuat.Image = ((System.Drawing.Image)(resources.GetObject("BttXuat.Image")));
            this.BttXuat.Location = new System.Drawing.Point(390, 68);
            this.BttXuat.Name = "BttXuat";
            this.BttXuat.Size = new System.Drawing.Size(41, 34);
            this.BttXuat.TabIndex = 11;
            // 
            // BttThem
            // 
            this.BttThem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttThem.Image = ((System.Drawing.Image)(resources.GetObject("BttThem.Image")));
            this.BttThem.Location = new System.Drawing.Point(47, 65);
            this.BttThem.Name = "BttThem";
            this.BttThem.Size = new System.Drawing.Size(40, 34);
            this.BttThem.TabIndex = 10;
            this.BttThem.Click += new System.EventHandler(this.BttThem_Click);
            // 
            // BttIn
            // 
            this.BttIn.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttIn.Image = ((System.Drawing.Image)(resources.GetObject("BttIn.Image")));
            this.BttIn.Location = new System.Drawing.Point(299, 65);
            this.BttIn.Name = "BttIn";
            this.BttIn.Size = new System.Drawing.Size(39, 34);
            this.BttIn.TabIndex = 12;
            this.BttIn.Click += new System.EventHandler(this.BttIn_Click);
            // 
            // BttXoa
            // 
            this.BttXoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttXoa.Image = ((System.Drawing.Image)(resources.GetObject("BttXoa.Image")));
            this.BttXoa.Location = new System.Drawing.Point(108, 65);
            this.BttXoa.Name = "BttXoa";
            this.BttXoa.Size = new System.Drawing.Size(39, 34);
            this.BttXoa.TabIndex = 9;
            this.BttXoa.Click += new System.EventHandler(this.BttXoa_Click);
            // 
            // BttNapLai
            // 
            this.BttNapLai.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttNapLai.Image = ((System.Drawing.Image)(resources.GetObject("BttNapLai.Image")));
            this.BttNapLai.Location = new System.Drawing.Point(236, 65);
            this.BttNapLai.Name = "BttNapLai";
            this.BttNapLai.Size = new System.Drawing.Size(41, 34);
            this.BttNapLai.TabIndex = 13;
            this.BttNapLai.Click += new System.EventHandler(this.BttNapLai_Click);
            // 
            // BttSua
            // 
            this.BttSua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttSua.Image = ((System.Drawing.Image)(resources.GetObject("BttSua.Image")));
            this.BttSua.Location = new System.Drawing.Point(166, 65);
            this.BttSua.Name = "BttSua";
            this.BttSua.Size = new System.Drawing.Size(41, 34);
            this.BttSua.TabIndex = 8;
            this.BttSua.Click += new System.EventHandler(this.BttSua_Click);
          
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Controls.Add(this.BttIn);
            this.groupControl1.Controls.Add(this.BttThem);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.BttSua);
            this.groupControl1.Controls.Add(this.BttXoa);
            this.groupControl1.Controls.Add(this.BttNapLai);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1012, 128);
            this.groupControl1.TabIndex = 15;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Vai Trò";
            this.gridColumn1.FieldName = "VaiTro";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // UCPhanQuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.BttXuat);
            this.Controls.Add(this.GridTaiKhoan);
            this.Name = "UCPhanQuyen";
            this.Size = new System.Drawing.Size(1012, 479);
            this.Load += new System.EventHandler(this.UCPhanQuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridTaiKhoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl GridTaiKhoan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton BttXuat;
        private DevExpress.XtraEditors.SimpleButton BttThem;
        private DevExpress.XtraEditors.SimpleButton BttIn;
        private DevExpress.XtraEditors.SimpleButton BttXoa;
        private DevExpress.XtraEditors.SimpleButton BttNapLai;
        private DevExpress.XtraEditors.SimpleButton BttSua;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;

    }
}
