﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;
using DTO;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class NhanVien_Themcs : DevExpress.XtraEditors.XtraForm
    {
        public NhanVien_Themcs()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            NhanVienDTO nv = new NhanVienDTO();
            if (txtMaNhanVien.Text == "")
            {
                dxErrorProvider1.SetError(txtMaNhanVien, "Vui lòng nhập thông tin");
            }
            else if (txtTenNV.Text == "")
            {
                dxErrorProvider1.SetError(txtTenNV, "Vui lòng nhập thông tin");
            }
            try
            {
                nv.Manv = txtMaNhanVien.Text;
                nv.Tennv = txtTenNV.Text;
                nv.Ngaysinh = dateEditNgaySinh.DateTime;
                nv.Cmnd = txtCMND.Text;
                nv.Diachi = txtDiaChi.Text;
                nv.Dienthoai = txtDienThoai.Text;
                nv.Maga = gridLookUpGa.Text;
                nv.Loai = gridLookUpLoaiNV.Text;
                nv.Nvquanly = GridLookupNVQL.Text;
                


                NhanVienBUS.ThemNV(nv);
            }
            catch
            {
            }
        }

        public void bingGa()
        {
            BindingSource bing = new BindingSource();
            DataTable dsGa = new DataTable();
            dsGa = GaBUS.LayGa();
            bing.DataSource = dsGa;

            gridLookUpGa.Properties.DataSource = bing;
            gridLookUpGa.Properties.DisplayMember = "Tenga";
            gridLookUpGa.Properties.ValueMember = "Maga";

        }
        public void bingLoaiNV()
        {
            BindingSource bing = new BindingSource();
            DataTable dsLoaiNV = new DataTable();
            dsLoaiNV = LoaiNhanVienBUS.LayLoaiNhanVien();
            bing.DataSource = dsLoaiNV;

            gridLookUpLoaiNV.Properties.DataSource = bing;
            gridLookUpLoaiNV.Properties.DisplayMember = "tenvt";
            gridLookUpLoaiNV.Properties.ValueMember = "Maloainv";
        }
        public void bingNVQL()
        {
            BindingSource bing = new BindingSource();
            DataTable dsNV = new DataTable();
            dsNV = NhanVienBUS.LayBangNhanvien();
            bing.DataSource = dsNV;

            GridLookupNVQL.Properties.DataSource = bing;
            GridLookupNVQL.Properties.DisplayMember = "Tennv";
            GridLookupNVQL.Properties.ValueMember = "Manv";
        }

        private void NhanVien_Themcs_Load(object sender, EventArgs e)
        {
            bingGa();
            bingLoaiNV();
            bingNVQL();
        }
        
    }
}