﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class ChuyenTau_Them : DevExpress.XtraEditors.XtraForm
    {
        public delegate void loadDS();
        public loadDS load;
        public ChuyenTau_Them()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            ChuyenTauDTO chuyentau = new ChuyenTauDTO();

            DateTime valueBD = TGBD.Value.Date +
                    timeTGBD.Value.TimeOfDay;
            DateTime valueKT = TGKT.Value.Date +
                    timeTGKT.Value.TimeOfDay;

            chuyentau.Thoigiankhoihanh = valueBD;
            chuyentau.Thoigianden = valueKT;
            chuyentau.STT = int.Parse(STT.Text);
            chuyentau.Matuyen = gridLookUpTuyen.EditValue.ToString();
            ChuyenTauBUS.ThemChuyenTau(chuyentau);
            load();
            this.Close();
        }
        public void BingTuyen()
        {
            BindingSource bing = new BindingSource();
            DataTable dsTuyen = new DataTable();
            dsTuyen = TuyenBUS.LayBangTuyen();
            bing.DataSource = dsTuyen;

            gridLookUpTuyen.Properties.DataSource = bing;
            gridLookUpTuyen.Properties.DisplayMember = "TENTD";
            gridLookUpTuyen.Properties.ValueMember = "MATD";

        }

        private void ChuyenTau_Them_Load(object sender, EventArgs e)
        {
            BingTuyen();
        }

        private void btThoat_Click(object sender, EventArgs e)
        {

        }

        private void labelControl5_Click(object sender, EventArgs e)
        {

        }
    }
}