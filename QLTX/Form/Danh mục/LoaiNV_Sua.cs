﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class LoaiNV_Sua : DevExpress.XtraEditors.XtraForm
    {
        public LoaiNV_Sua()
        {
            InitializeComponent();
        }
        public LoaiNhanVienDTO loainv = new LoaiNhanVienDTO();

        private void LoaiNV_Sua_Load(object sender, EventArgs e)
        {
            txtChuan1.Text = loainv.Maloainv;
            txtChuan2.Text  = loainv.Tenvt;
            txtchuan3.Text  = loainv.Ghichu;
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            LoaiNhanVienDTO Lnv = new LoaiNhanVienDTO();
            Lnv.Maloainv = txtChuan1.Text;
            Lnv.Tenvt = txtChuan2.Text;
            Lnv.Ghichu = txtchuan3.Text;
            LoaiNhanVienBUS.SuaLoaiNV(Lnv);
        }

    }
}