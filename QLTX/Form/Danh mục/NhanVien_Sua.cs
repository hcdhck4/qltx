﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using DTO;
using BUS;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class NhanVien_Sua : DevExpress.XtraEditors.XtraForm
    {
        public NhanVien_Sua()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            NhanVienDTO nv = new NhanVienDTO();
            if (txtMaNhanVien.Text == "")
            {
                dxErrorProvider1.SetError(txtMaNhanVien, "Vui lòng nhập thông tin");
            }
            else if (txtTenNV.Text == "")
            {
                dxErrorProvider1.SetError(txtTenNV, "Vui lòng nhập thông tin");
            }
                nv.Manv = txtMaNhanVien.Text;
                nv.Tennv = txtTenNV.Text;
                nv.Ngaysinh = dateEditNgaySinh.DateTime;
                nv.Cmnd = txtCMND.Text;
                nv.Diachi = txtDiaChi.Text;
                nv.Dienthoai = txtDienThoai.Text;
                nv.Maga = gridLookUpGa.Text;
                nv.Loai = gridLookUpLNV.Text;
                nv.Nvquanly = GridLookupNVQL.Text;
                NhanVienBUS.SuaNV(nv);
        
        }
    }
}