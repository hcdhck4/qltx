﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class HanhTrinh_Sua : DevExpress.XtraEditors.XtraForm
    {
        public HanhTrinh_Sua()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            HanhTrinhDTO hanhtrinh = new HanhTrinhDTO();
            if (txtMaHanhTrinh.Text == "")
            {
                dxErrorProvider1.SetError(txtMaHanhTrinh, "Vui lòng nhập thông tin");
                return;
            }
            hanhtrinh.Maga = gridLookUpGa.Text;
            hanhtrinh.Matuyen = gridLookUpTuyen.Text;
            hanhtrinh.Donvitinh = gridLookUpDVT.Text;
            hanhtrinh.Mahanhtrinh = txtMaHanhTrinh.Text;
            //hanhtrinh.Thoidiemxuatphat = dateThoiDiemXP.time;
        }
        public void BingGa()
        {
            BindingSource bing = new BindingSource();
            DataTable dsGa = new DataTable();
            dsGa = GaBUS.LayGa();
            bing.DataSource = dsGa;

            gridLookUpGa.Properties.DataSource = bing;
            gridLookUpGa.Properties.DisplayMember = "Tenga";
            gridLookUpGa.Properties.ValueMember = "Maga";

        }

        public void BingTuyen()
        {
            BindingSource bing = new BindingSource();
            DataTable dsTuyen = new DataTable();
            dsTuyen = TuyenBUS.LayBangTuyen();
            bing.DataSource = dsTuyen;

            gridLookUpTuyen.Properties.DataSource = bing;
            gridLookUpTuyen.Properties.DisplayMember = "Tentuyen";
            gridLookUpTuyen.Properties.ValueMember = "Matuyen";

        }

        public void BingDVT()
        {
            BindingSource bing = new BindingSource();
            DataTable dsDVT = new DataTable();
            dsDVT = DonViTinhBUS.LayBangDVTinh();
            bing.DataSource = dsDVT;

            gridLookUpDVT.Properties.DataSource = bing;
            gridLookUpDVT.Properties.DisplayMember = "Ten";
            gridLookUpDVT.Properties.ValueMember = "Madv";

        }

        private void HanhTrinh_Them_Load(object sender, EventArgs e)
        {
            BingDVT();
            BingGa();
            BingTuyen();
        }
    }
}