﻿namespace QLDuongSat.Form.Danh_mục
{
    partial class ChuyenTau_Sua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChuyenTau_Sua));
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpTuyen = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.timeTGKT = new System.Windows.Forms.DateTimePicker();
            this.timeTGBD = new System.Windows.Forms.DateTimePicker();
            this.TGKT = new System.Windows.Forms.DateTimePicker();
            this.TGBD = new System.Windows.Forms.DateTimePicker();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.STT = new System.Windows.Forms.TextBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btThoat = new DevExpress.XtraEditors.SimpleButton();
            this.btLuu = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpTuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl6);
            this.panelControl3.Controls.Add(this.gridLookUpTuyen);
            this.panelControl3.Location = new System.Drawing.Point(3, 45);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(404, 49);
            this.panelControl3.TabIndex = 28;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl6.Location = new System.Drawing.Point(20, 18);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(35, 16);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Tuyến";
            // 
            // gridLookUpTuyen
            // 
            this.gridLookUpTuyen.Location = new System.Drawing.Point(71, 17);
            this.gridLookUpTuyen.Name = "gridLookUpTuyen";
            this.gridLookUpTuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpTuyen.Properties.NullText = "";
            this.gridLookUpTuyen.Properties.View = this.gridView1;
            this.gridLookUpTuyen.Size = new System.Drawing.Size(133, 20);
            this.gridLookUpTuyen.TabIndex = 25;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Location = new System.Drawing.Point(3, 12);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(404, 27);
            this.panelControl2.TabIndex = 27;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelControl4.Location = new System.Drawing.Point(139, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(109, 16);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "SỬA CHUYẾN TÀU";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.timeTGKT);
            this.panelControl1.Controls.Add(this.timeTGBD);
            this.panelControl1.Controls.Add(this.TGKT);
            this.panelControl1.Controls.Add(this.TGBD);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.STT);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.btThoat);
            this.panelControl1.Controls.Add(this.btLuu);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Location = new System.Drawing.Point(3, 100);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(404, 184);
            this.panelControl1.TabIndex = 29;
            // 
            // timeTGKT
            // 
            this.timeTGKT.CustomFormat = "hh:ss:mm";
            this.timeTGKT.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeTGKT.Location = new System.Drawing.Point(292, 98);
            this.timeTGKT.Name = "timeTGKT";
            this.timeTGKT.ShowUpDown = true;
            this.timeTGKT.Size = new System.Drawing.Size(85, 21);
            this.timeTGKT.TabIndex = 17;
            // 
            // timeTGBD
            // 
            this.timeTGBD.CustomFormat = "hh:ss:mm";
            this.timeTGBD.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeTGBD.Location = new System.Drawing.Point(292, 61);
            this.timeTGBD.Name = "timeTGBD";
            this.timeTGBD.ShowUpDown = true;
            this.timeTGBD.Size = new System.Drawing.Size(85, 21);
            this.timeTGBD.TabIndex = 16;
            // 
            // TGKT
            // 
            this.TGKT.CustomFormat = "MM/dd/yyyy";
            this.TGKT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TGKT.Location = new System.Drawing.Point(177, 98);
            this.TGKT.Name = "TGKT";
            this.TGKT.Size = new System.Drawing.Size(109, 21);
            this.TGKT.TabIndex = 15;
            // 
            // TGBD
            // 
            this.TGBD.CustomFormat = "MM/dd/yyyy";
            this.TGBD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TGBD.Location = new System.Drawing.Point(178, 61);
            this.TGBD.Name = "TGBD";
            this.TGBD.Size = new System.Drawing.Size(108, 21);
            this.TGBD.TabIndex = 14;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl1.Location = new System.Drawing.Point(20, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(24, 16);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "STT";
            // 
            // STT
            // 
            this.STT.Location = new System.Drawing.Point(179, 22);
            this.STT.Name = "STT";
            this.STT.Size = new System.Drawing.Size(198, 21);
            this.STT.TabIndex = 12;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl5.Location = new System.Drawing.Point(20, 98);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(81, 16);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Thời Gian Đến";
            // 
            // btThoat
            // 
            this.btThoat.Image = ((System.Drawing.Image)(resources.GetObject("btThoat.Image")));
            this.btThoat.Location = new System.Drawing.Point(302, 145);
            this.btThoat.Name = "btThoat";
            this.btThoat.Size = new System.Drawing.Size(75, 25);
            this.btThoat.TabIndex = 8;
            this.btThoat.Text = "Thoát";
            this.btThoat.Click += new System.EventHandler(this.btThoat_Click_1);
            // 
            // btLuu
            // 
            this.btLuu.Image = ((System.Drawing.Image)(resources.GetObject("btLuu.Image")));
            this.btLuu.Location = new System.Drawing.Point(211, 145);
            this.btLuu.Name = "btLuu";
            this.btLuu.Size = new System.Drawing.Size(75, 25);
            this.btLuu.TabIndex = 7;
            this.btLuu.Text = "Lưu";
            this.btLuu.Click += new System.EventHandler(this.btLuu_Click_1);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.labelControl3.Location = new System.Drawing.Point(20, 61);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(115, 16);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Thời Gian Khởi Hành";
            // 
            // ChuyenTau_Sua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 290);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Name = "ChuyenTau_Sua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ChuyenTau_Sua_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpTuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpTuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.DateTimePicker timeTGKT;
        private System.Windows.Forms.DateTimePicker timeTGBD;
        private System.Windows.Forms.DateTimePicker TGKT;
        private System.Windows.Forms.DateTimePicker TGBD;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TextBox STT;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btThoat;
        private DevExpress.XtraEditors.SimpleButton btLuu;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}