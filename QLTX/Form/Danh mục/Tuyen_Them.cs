﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class Tuyen_Them : DevExpress.XtraEditors.XtraForm
    {
        public delegate void loadDS();
        public loadDS load;
        public Tuyen_Them()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            TuyenDTO tuyen = new TuyenDTO();
            if (txtMaTuyen.Text == "")
            {
                dxErrorProvider1.SetError(txtMaTuyen, "Vui lòng nhập thông tin");
            }
            else if (txtTenTuyen.Text == "")
            {
                dxErrorProvider1.SetError(txtTenTuyen, "Vui lòng nhập thông tin");
            }
            tuyen.Matuyen = txtMaTuyen.Text;
            tuyen.Tentuyen = txtTenTuyen.Text;
            tuyen.Chieudai = txtChieuDai.Text;
            TuyenBUS.ThemTuyen(tuyen);
            load();
            this.Close();
        }

        private void btThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}