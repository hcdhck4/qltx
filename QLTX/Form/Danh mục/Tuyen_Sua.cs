﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using BUS;
using DTO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QLDuongSat.UC_Danh_Muc;
namespace QLDuongSat.Form.Danh_mục
{
    public partial class Tuyen_Sua : DevExpress.XtraEditors.XtraForm
    {
        public TuyenDTO suaTuyen = new TuyenDTO();
        public delegate void loadDS();
        public loadDS load;
        public Tuyen_Sua()
        {
            InitializeComponent();
        }

        private void btLuu_Click(object sender, EventArgs e)
        {
            TuyenDTO tuyen = new TuyenDTO();
            if (txtMaTuyen.Text == "")
            {
                dxErrorProvider1.SetError(txtMaTuyen, "Vui lòng nhập thông tin");
            }
            else if (txtTenTuyen.Text == "")
            {
                dxErrorProvider1.SetError(txtTenTuyen, "Vui lòng nhập thông tin");
            }
            tuyen.Matuyen = txtMaTuyen.Text;
            tuyen.Tentuyen = txtTenTuyen.Text;
            tuyen.Chieudai = txtChieuDai.Text;
            TuyenBUS.SuaTuyen(tuyen);
            load();
            this.Close();
        }

        private void Tuyen_Sua_Load(object sender, EventArgs e)
        {
            txtMaTuyen.Text = suaTuyen.Matuyen;
            txtTenTuyen.Text = suaTuyen.Tentuyen;
            txtChieuDai.Text = suaTuyen.Chieudai;
        }

        private void btThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}