﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DTO;
using BUS;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class LoaiNV_Them : DevExpress.XtraEditors.XtraForm
    {
        public LoaiNV_Them()
        {
            InitializeComponent();
        }
        
        private void btLuu_Click(object sender, EventArgs e)
        {
             LoaiNhanVienDTO loainv = new LoaiNhanVienDTO();
            if (txtChuan1.Text == "")
            {
                dxErrorProvider1.SetError(txtChuan1, "Vui lòng nhập thông tin");
            }
            else if (txtChuan2.Text == "")
            {
                dxErrorProvider1.SetError(txtChuan2, "Vui lòng nhập thông tin");
            }
            try
            {
                loainv.Maloainv = txtChuan1.Text;
                loainv.Tenvt = txtChuan2.Text;
                loainv.Ghichu = txtchuan3.Text;
                LoaiNhanVienBUS.ThemLoaiNV(loainv);
            }
            catch
            {
            }
            
        }
    }
}