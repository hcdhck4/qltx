﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using DTO;
using BUS;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QLDuongSat.Form.Danh_mục
{
    public partial class ChuyenTau_Sua : DevExpress.XtraEditors.XtraForm
    {
        public ChuyenTauDTO suaChuyen = new ChuyenTauDTO();
        public delegate void loadDS();
        public loadDS load;
        public ChuyenTau_Sua()
        {
            InitializeComponent();
        }

        public void BingTuyen()
        {
            BindingSource bing = new BindingSource();
            DataTable dsTuyen = new DataTable();
            dsTuyen = TuyenBUS.LayBangTuyen();
            bing.DataSource = dsTuyen;

            gridLookUpTuyen.Properties.DataSource = bing;
            gridLookUpTuyen.Properties.DisplayMember = "TENTD";
            gridLookUpTuyen.Properties.ValueMember = "MATD";
            gridLookUpTuyen.EditValue = suaChuyen.Matuyen;

        }

        private void btLuu_Click_1(object sender, EventArgs e)
        {
            ChuyenTauDTO chuyentau = new ChuyenTauDTO();
            DateTime valueBD = TGBD.Value.Date +              
                timeTGBD.Value.TimeOfDay;
            DateTime valueKT = TGKT.Value.Date +
                    timeTGKT.Value.TimeOfDay;

            chuyentau.Thoigiankhoihanh = valueBD;
            chuyentau.Thoigianden = valueKT;
            chuyentau.STT = int.Parse(STT.Text);
            chuyentau.Matuyen = gridLookUpTuyen.EditValue.ToString();
            ChuyenTauBUS.SuaChuyenTau(chuyentau);
            load();
            this.Close();
        }

        private void ChuyenTau_Sua_Load(object sender, EventArgs e)
        {
            STT.Text = suaChuyen.STT.ToString();
            TGBD.Value = suaChuyen.Thoigiankhoihanh;
            TGKT.Value = suaChuyen.Thoigianden;
            timeTGBD.Value = suaChuyen.Thoigiankhoihanh;
            timeTGKT.Value = suaChuyen.Thoigianden;
            BingTuyen();
        }

        private void btThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}