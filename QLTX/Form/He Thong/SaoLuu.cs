﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DAO;

namespace QLDuongSat.Form.He_Thong
{
    public partial class SaoLuu : DevExpress.XtraEditors.XtraForm
    {
        public SaoLuu()
        {
            InitializeComponent();
        }

        private void SaoLuu_Load(object sender, EventArgs e)
        {
            TxtTen.Text = "saoluu.bak";
        }

        private void BttDuongDan_Click(object sender, EventArgs e)
        {
             SaveFileDialog save = new  SaveFileDialog();
            save.Filter = "File Type (*.bak)|*.bak";
            save.DefaultExt = "bak";
            save.FileName = TxtTen.Text;
            if (save.ShowDialog() == DialogResult.OK)
            {
                TxtDuongDan.Text = save.FileName;
            }
        }

        private void BttDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BttThucHien_Click(object sender, EventArgs e)
        {
            String st = TxtDuongDan.Text;
            SqlConnection cn = DataProvider.Connection();
            try
            {
                SqlCommand cmd = new SqlCommand("BACKUP DATABASE QLDUONGSAT TO DISK = '" + st + "' WITH INIT", cn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Sao lưu thàng công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Sao lưu thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.Close();
        }
    }
}