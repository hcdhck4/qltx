﻿namespace QLDuongSat.Form.He_Thong
{
    partial class SaoLuu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaoLuu));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.BttDuongDan = new DevExpress.XtraEditors.SimpleButton();
            this.BttDong = new DevExpress.XtraEditors.SimpleButton();
            this.BttThucHien = new DevExpress.XtraEditors.SimpleButton();
            this.TxtTen = new DevExpress.XtraEditors.TextEdit();
            this.TxtDuongDan = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDuongDan.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.BttDuongDan);
            this.groupControl1.Controls.Add(this.BttDong);
            this.groupControl1.Controls.Add(this.BttThucHien);
            this.groupControl1.Controls.Add(this.TxtTen);
            this.groupControl1.Controls.Add(this.TxtDuongDan);
            this.groupControl1.Controls.Add(this.LabelControl3);
            this.groupControl1.Controls.Add(this.LabelControl2);
            this.groupControl1.Location = new System.Drawing.Point(1, 1);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(439, 204);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Sao Lưu Dữ Liệu";
            // 
            // BttDuongDan
            // 
            this.BttDuongDan.Image = ((System.Drawing.Image)(resources.GetObject("BttDuongDan.Image")));
            this.BttDuongDan.Location = new System.Drawing.Point(372, 93);
            this.BttDuongDan.Name = "BttDuongDan";
            this.BttDuongDan.Size = new System.Drawing.Size(25, 21);
            this.BttDuongDan.TabIndex = 19;
            this.BttDuongDan.Click += new System.EventHandler(this.BttDuongDan_Click);
            // 
            // BttDong
            // 
            this.BttDong.Image = ((System.Drawing.Image)(resources.GetObject("BttDong.Image")));
            this.BttDong.Location = new System.Drawing.Point(267, 146);
            this.BttDong.Name = "BttDong";
            this.BttDong.Size = new System.Drawing.Size(84, 41);
            this.BttDong.TabIndex = 17;
            this.BttDong.Text = "Đóng";
            this.BttDong.Click += new System.EventHandler(this.BttDong_Click);
            // 
            // BttThucHien
            // 
            this.BttThucHien.Image = ((System.Drawing.Image)(resources.GetObject("BttThucHien.Image")));
            this.BttThucHien.Location = new System.Drawing.Point(147, 146);
            this.BttThucHien.Name = "BttThucHien";
            this.BttThucHien.Size = new System.Drawing.Size(96, 41);
            this.BttThucHien.TabIndex = 18;
            this.BttThucHien.Text = "Thực Hiện";
            this.BttThucHien.Click += new System.EventHandler(this.BttThucHien_Click);
            // 
            // TxtTen
            // 
            this.TxtTen.Location = new System.Drawing.Point(102, 40);
            this.TxtTen.Name = "TxtTen";
            this.TxtTen.Size = new System.Drawing.Size(295, 20);
            this.TxtTen.TabIndex = 16;
            // 
            // TxtDuongDan
            // 
            this.TxtDuongDan.Location = new System.Drawing.Point(102, 94);
            this.TxtDuongDan.Name = "TxtDuongDan";
            this.TxtDuongDan.Size = new System.Drawing.Size(295, 20);
            this.TxtDuongDan.TabIndex = 15;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl3.Location = new System.Drawing.Point(19, 93);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(77, 18);
            this.LabelControl3.TabIndex = 13;
            this.LabelControl3.Text = "Đường dẫn:";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl2.Location = new System.Drawing.Point(19, 42);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(77, 18);
            this.LabelControl2.TabIndex = 14;
            this.LabelControl2.Text = "Tên tập tin:";
            // 
            // SaoLuu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 206);
            this.Controls.Add(this.groupControl1);
            this.Name = "SaoLuu";
            this.Text = "Sao lưu dữ liệu";
            this.Load += new System.EventHandler(this.SaoLuu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDuongDan.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        internal DevExpress.XtraEditors.SimpleButton BttDuongDan;
        internal DevExpress.XtraEditors.SimpleButton BttDong;
        internal DevExpress.XtraEditors.SimpleButton BttThucHien;
        internal DevExpress.XtraEditors.TextEdit TxtTen;
        internal DevExpress.XtraEditors.TextEdit TxtDuongDan;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
    }
}