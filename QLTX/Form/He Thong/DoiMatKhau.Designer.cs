﻿namespace QLDuongSat.Form.He_Thong
{
    partial class DoiMatKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoiMatKhau));
            this.BttThoat = new DevExpress.XtraEditors.SimpleButton();
            this.BttDongY = new DevExpress.XtraEditors.SimpleButton();
            this.TxtNhapLai = new DevExpress.XtraEditors.TextEdit();
            this.TxtMoi = new DevExpress.XtraEditors.TextEdit();
            this.TxtCu = new DevExpress.XtraEditors.TextEdit();
            this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNhapLai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMoi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl1)).BeginInit();
            this.PanelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BttThoat
            // 
            this.BttThoat.Image = ((System.Drawing.Image)(resources.GetObject("BttThoat.Image")));
            this.BttThoat.Location = new System.Drawing.Point(247, 170);
            this.BttThoat.Name = "BttThoat";
            this.BttThoat.Size = new System.Drawing.Size(90, 49);
            this.BttThoat.TabIndex = 4;
            this.BttThoat.Text = "Thoát";
            this.BttThoat.Click += new System.EventHandler(this.BttThoat_Click);
            // 
            // BttDongY
            // 
            this.BttDongY.Image = ((System.Drawing.Image)(resources.GetObject("BttDongY.Image")));
            this.BttDongY.Location = new System.Drawing.Point(105, 170);
            this.BttDongY.Name = "BttDongY";
            this.BttDongY.Size = new System.Drawing.Size(90, 49);
            this.BttDongY.TabIndex = 3;
            this.BttDongY.Text = "Đồng Ý";
            this.BttDongY.Click += new System.EventHandler(this.BttDongY_Click);
            // 
            // TxtNhapLai
            // 
            this.TxtNhapLai.Location = new System.Drawing.Point(162, 127);
            this.TxtNhapLai.Name = "TxtNhapLai";
            this.TxtNhapLai.Properties.PasswordChar = '*';
            this.TxtNhapLai.Size = new System.Drawing.Size(239, 20);
            this.TxtNhapLai.TabIndex = 2;
            // 
            // TxtMoi
            // 
            this.TxtMoi.Location = new System.Drawing.Point(162, 93);
            this.TxtMoi.Name = "TxtMoi";
            this.TxtMoi.Properties.PasswordChar = '*';
            this.TxtMoi.Size = new System.Drawing.Size(239, 20);
            this.TxtMoi.TabIndex = 1;
            // 
            // TxtCu
            // 
            this.TxtCu.Location = new System.Drawing.Point(162, 59);
            this.TxtCu.Name = "TxtCu";
            this.TxtCu.Properties.PasswordChar = '*';
            this.TxtCu.Size = new System.Drawing.Size(239, 20);
            this.TxtCu.TabIndex = 0;
            // 
            // PanelControl1
            // 
            this.PanelControl1.Controls.Add(this.BttThoat);
            this.PanelControl1.Controls.Add(this.BttDongY);
            this.PanelControl1.Controls.Add(this.TxtNhapLai);
            this.PanelControl1.Controls.Add(this.TxtMoi);
            this.PanelControl1.Controls.Add(this.TxtCu);
            this.PanelControl1.Controls.Add(this.LabelControl3);
            this.PanelControl1.Controls.Add(this.LabelControl4);
            this.PanelControl1.Controls.Add(this.LabelControl2);
            this.PanelControl1.Controls.Add(this.LabelControl1);
            this.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelControl1.Location = new System.Drawing.Point(0, 0);
            this.PanelControl1.Name = "PanelControl1";
            this.PanelControl1.Size = new System.Drawing.Size(440, 230);
            this.PanelControl1.TabIndex = 1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl3.Location = new System.Drawing.Point(12, 60);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(84, 16);
            this.LabelControl3.TabIndex = 1;
            this.LabelControl3.Text = "Mật khẩu cũ:";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl4.Location = new System.Drawing.Point(12, 128);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(119, 16);
            this.LabelControl4.TabIndex = 1;
            this.LabelControl4.Text = "Nhập lại mật khẩu:";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl2.Location = new System.Drawing.Point(12, 94);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(91, 16);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Mật khẩu mới:";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl1.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LabelControl1.Location = new System.Drawing.Point(121, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(176, 24);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Thay Đổi Mật Khẩu";
            // 
            // DoiMatKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 230);
            this.Controls.Add(this.PanelControl1);
            this.Name = "DoiMatKhau";
            this.Text = "Thay Đổi Mật Khẩu";
            ((System.ComponentModel.ISupportInitialize)(this.TxtNhapLai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMoi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl1)).EndInit();
            this.PanelControl1.ResumeLayout(false);
            this.PanelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.SimpleButton BttThoat;
        internal DevExpress.XtraEditors.SimpleButton BttDongY;
        internal DevExpress.XtraEditors.TextEdit TxtNhapLai;
        internal DevExpress.XtraEditors.TextEdit TxtMoi;
        internal DevExpress.XtraEditors.TextEdit TxtCu;
        internal DevExpress.XtraEditors.PanelControl PanelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}