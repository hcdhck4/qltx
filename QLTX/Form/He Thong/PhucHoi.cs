﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace QLDuongSat.Form.He_Thong
{
    public partial class PhucHoi : DevExpress.XtraEditors.XtraForm
    {
        public PhucHoi()
        {
            InitializeComponent();
        }

        private void BttDuongDan_Click(object sender, EventArgs e)
        {
        OpenFileDialog  open = new OpenFileDialog();
        open.Filter = "File Type (*.bak)|*.bak";
        open.DefaultExt = "bak";
        if( open.ShowDialog() == DialogResult.OK )
        {
            TxtDuongDan.Text = open.FileName;
        }        
        }

        private void BttThucHien_Click(object sender, EventArgs e)
        {
             string st = TxtDuongDan.Text;
             SqlConnection cnn = new SqlConnection("Data Source=MYPC\\SQLEXPRESS;Database=QLBH;integrated security=SSPI;");
        cnn.Open();
        try 
	    {	        
		   SqlCommand   cmd = new   SqlCommand("RESTORE DATABASE QLDUONGSAT TO DISK = '" + st + "' WITH INIT", cnn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Phục hồi thàng công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
	    }
	    catch (Exception)
	    {
		
		      MessageBox.Show("Phục hồi thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
	    }
                               
        }

        private void BttDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}