﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BUS;

namespace QLDuongSat.Form.He_Thong
{
    
    public partial class DoiMatKhau : DevExpress.XtraEditors.XtraForm
    {
        public DoiMatKhau()
        {
            InitializeComponent();
        }
          //Khai báo biến đổi mật khẩu    
    public string matkhau= FrmDangNhap.matkhau;
    public int Matk;
private void BttDongY_Click(object sender, EventArgs e)
    {
        if (TxtCu.Text == matkhau)
        {
            if (TxtMoi.Text == TxtNhapLai.Text)
            {
                //Gọi hàm cập nhật tài khoản
                VaiTroBus.DoiMatKhau(Matk, TxtMoi.Text);
                MessageBox.Show("Thay đổi thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Mật khẩu nhập lại không khớp!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        else { MessageBox.Show("Mật khẩu cũ không hợp lệ!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
    }

    private void BttThoat_Click(object sender, EventArgs e)
    {
        this.Close();
    }
    }
}