﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using AddTab;
using QLDuongSat.UC_Danh_Muc;
using System.Threading;
using DevExpress.XtraSplashScreen;
using QLDuongSat.UC_He_Thong;
using QLDuongSat.Form.He_Thong;
using System.Diagnostics;
namespace QLTX

{
    using QLDuongSat;
    public partial class FromMain : RibbonForm
    {
      
        AddTab.TabAdd clsAddTab = new TabAdd();
        

        public FromMain()
        {
            InitializeComponent();
            InitSkinGallery();           

        }
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
        void PhanQuyen()
        {
            string ss = FrmDangNhap.Vaitro.Trim();
            //Kiểm tra  quyền thấy vào hệ thống phần mềm.
            if (ss.Equals("Nhân Viên Tin Học"))
            {
                RipDanhMuc.Visible = false;
                RipChucNang.Visible = false;
            }
            else if (ss.Equals("Quản Lý"))
            {
                BttSaoLuu.Enabled = false;
                BttPhucHoi.Enabled = false;
            }
            else if (ss.Equals("Nhân Viên Bán Vé"))
            {
                BttSaoLuu.Enabled = false;
                BttPhucHoi.Enabled = false;
                RipDanhMuc.Visible = false;
                BttThongKe.Enabled = false;
            }
            else if (ss.Equals("Khách Online"))
            {
                BttSaoLuu.Enabled = false;
                BttPhucHoi.Enabled = false;
                RipDanhMuc.Visible = false;
                BttThongKe.Enabled = false;
            }            
        }
        private void FromMain_Load(object sender, EventArgs e)
        {
            Thread.Sleep(1000);            
        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            //dong tab
            DevExpress.XtraTab.XtraTabControl tabControl = sender as DevExpress.XtraTab.XtraTabControl;
            DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs arg = e as DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs;
            (arg.Page as DevExpress.XtraTab.XtraTabPage).Dispose();

        }

        private void xtraTabControl1_ControlAdded(object sender, ControlEventArgs e)
        {
            // Khi add vào thì Focus tới ngay Tab vừa Add
            xtraTabControl1.SelectedTabPageIndex = xtraTabControl1.TabPages.Count - 1;          
        }

        private void BttTuyen_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Tuyến")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {
                // chuyển focus tới TAb Tuyến
            }
            else
            {// Nếu chưa có TAb này thì gọi hàm Addtab xây dựng ở trên để Add Tab con vào
                timer1.Start();
                SplashScreenManager.ShowForm(typeof(WaitForm1));
                Tuyen frm = new Tuyen();
                frm.Dock = System.Windows.Forms.DockStyle.Fill;
                clsAddTab.AddTab(xtraTabControl1, "", "Tuyến", frm);
                SplashScreenManager.CloseForm();
            }
        }

        private void BttChuyenTau_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Chuyến Tàu")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {
                // chuyển focus tới TAb chuyến tàu
            }
            else
            {// Nếu chưa có TAb này thì gọi hàm Addtab xây dựng ở trên để Add Tab con vào
                timer1.Start();
                SplashScreenManager.ShowForm(typeof(WaitForm1));
                ChuyenTau frm = new ChuyenTau();
                frm.Dock = System.Windows.Forms.DockStyle.Fill;
                clsAddTab.AddTab(xtraTabControl1, "", "Chuyến Tàu", frm);
                SplashScreenManager.CloseForm();
            }
        }
        private void BttHanhTrinh_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Hành Trình")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {
                // chuyển focus tới TAb Tuyến
            }
            else
            {// Nếu chưa có TAb này thì gọi hàm Addtab xây dựng ở trên để Add Tab con vào
                timer1.Start();
                SplashScreenManager.ShowForm(typeof(WaitForm1));
                HanhTrinh frm = new HanhTrinh();
                frm.Dock = System.Windows.Forms.DockStyle.Fill;
                clsAddTab.AddTab(xtraTabControl1, "", "Hành Trình", frm);
                SplashScreenManager.CloseForm();
            }
        }

        private void iExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn muốn thoát chương trình?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void BttPhanQuyen_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Phân Quyền")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {
                // chuyển focus tới TAb Hành Khách
            }
            else
            {// Nếu chưa có TAb này thì gọi hàm Addtab xây dựng ở trên để Add Tab con vào
              
                SplashScreenManager.ShowForm(typeof(WaitForm1));
                UCPhanQuyen frm = new UCPhanQuyen();
                clsAddTab.AddTab(xtraTabControl1, "", "Phân Quyền", frm);
                SplashScreenManager.CloseForm();
            }
        }

        private void BttDoiMatKhau_ItemClick(object sender, ItemClickEventArgs e)
        {
            DoiMatKhau frmdoimk = new DoiMatKhau();
            frmdoimk.Matk = FrmDangNhap.Matk;
            frmdoimk.ShowDialog();
        }

        private void BttSaoLuu_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaoLuu frmsaoluu = new SaoLuu();
            frmsaoluu.ShowDialog();
        }

        private void BttPhucHoi_ItemClick(object sender, ItemClickEventArgs e)
        {
            PhucHoi  frmphuchoi = new PhucHoi ();
            frmphuchoi.ShowDialog();
        }

        private void BttHuongDan_ItemClick(object sender, ItemClickEventArgs e)
        {
            string path = System.IO.Directory.GetCurrentDirectory() + "\\HuongDan.pdf";
            Process.Start(path);
        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }        
    }
}