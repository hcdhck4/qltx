﻿using System;
using System.Data;
using System.Windows.Forms;
using BUS;
using DTO;
using QLDuongSat.Form.Danh_mục;
namespace QLDuongSat.UC_Danh_Muc
{
    public partial class HanhTrinh : UserControl
    {
        string MaHT = "";
        HanhTrinhDTO ht = new HanhTrinhDTO();

        public HanhTrinh()
        {
            InitializeComponent();
        }

        private void HanhTrinh_Load(object sender, EventArgs e)
        {
            DataTable bang = new DataTable();
            bang = HanhTrinhBUS.LayBangHanhTrinh();
            GridCTHanhTrinh.DataSource = bang;
        }

        private void BttThem_Click(object sender, EventArgs e)
        {
            HanhTrinh_Them frm = new HanhTrinh_Them();
            frm.ShowDialog();
        }

        private void BttSua_Click(object sender, EventArgs e)
        {
            HanhTrinh_Sua frm = new HanhTrinh_Sua();
            frm.ShowDialog();
        }

        private void BttNapLai_Click(object sender, EventArgs e)
        {
            HanhTrinh_Load(sender, e);
        }

        private void BttIn_Click(object sender, EventArgs e)
        {
            GridCTHanhTrinh.ShowPrintPreview();
        }

        private void BttXuat_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "File Excel (*.Xls)|*.Xls";
            save.FileName = "Excel";
            save.ShowDialog();
            if (save.FileName != "")
            {
                GridCTHanhTrinh.ExportToXls(save.FileName);
            }
        }

        private void GridCTHanhTrinh_Click(object sender, EventArgs e)
        {
            MaHT = gridView1.GetFocusedDisplayText();

            ht.Mahanhtrinh = (gridView1.GetFocusedRowCellValue(gridColumn1)).ToString();
            ht.Thoidiemxuatphat=(DateTime.Parse( gridView1.GetFocusedRowCellValue(gridColumn2).ToString()));
            ht.KhoangKm = (float.Parse(gridView1.GetFocusedRowCellValue(gridColumn3).ToString()));
            ht.Matuyen = (gridView1.GetFocusedRowCellValue(gridColumn4)).ToString();
            ht.Maga = (gridView1.GetFocusedRowCellValue(gridColumn5)).ToString();
           
        }

        private void BttXoa_Click(object sender, EventArgs e)
        {
            HanhTrinhBUS.XoaHanhTrinh(ht);
            MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK);
            {
                HanhTrinh_Load(sender,e);
            }

        }
    }
}
