﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BUS;
using DTO;
using QLDuongSat.Form.Danh_mục;

namespace QLDuongSat.UC_Danh_Muc
{
    public partial class Tuyen : UserControl
    {
        string Matuyen = "";
        public TuyenDTO tuyen = new TuyenDTO();

        public Tuyen()
        {
            InitializeComponent();
        }

        private void Tuyen_Load(object sender, EventArgs e)
        {
            DataTable bang = new DataTable();
            bang = TuyenBUS.LayBangTuyen();
            GridCTTuyen.DataSource = bang;
        }

        public void loadDS() {
            DataTable bang = new DataTable();
            bang = TuyenBUS.LayBangTuyen();
            GridCTTuyen.DataSource = bang;
        }

        private void BttThem_Click(object sender, EventArgs e)
        {
            Tuyen_Them them = new Tuyen_Them();
            them.load = new Tuyen_Them.loadDS(loadDS);
            them.ShowDialog();
        }

        private void BttNapLai_Click(object sender, EventArgs e)
        {
            Tuyen_Load(sender, e);
        }

        private void BttIn_Click(object sender, EventArgs e)
        {
            GridCTTuyen.ShowPrintPreview();
        }

        private void BttXuat_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "File Excel (*.Xls)|*.Xls";
            save.FileName = "Excel";
            save.ShowDialog();
            if (save.FileName != "")
            {
                GridCTTuyen.ExportToXls(save.FileName);
            }
        }

        private void GridCTTuyen_Click(object sender, EventArgs e)
        {
           Matuyen = gridView1.GetFocusedDisplayText();

           tuyen.Matuyen = (gridView1.GetFocusedRowCellValue(gridColumn1)).ToString();
           tuyen.Tentuyen = (gridView1.GetFocusedRowCellValue(gridColumn2)).ToString();
           tuyen.Chieudai = (gridView1.GetFocusedRowCellValue(gridColumn3)).ToString();
        }

        private void BttSua_Click(object sender, EventArgs e)
        {
            Tuyen_Sua frm1 = new Tuyen_Sua();
            frm1.suaTuyen = tuyen;
            frm1.load = new Tuyen_Sua.loadDS(loadDS);
            frm1.ShowDialog();
        }

        private void BttXoa_Click(object sender, EventArgs e)
        {
            TuyenBUS.XoaTuyen(tuyen);
            MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK);
            Tuyen_Load(sender, e);
        }



    }
}
