﻿namespace QLDuongSat.UC_Danh_Muc
{
    partial class ChuyenTau
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChuyenTau));
            this.GridCTChuyenTau = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.BttXuat = new DevExpress.XtraEditors.SimpleButton();
            this.BttThem = new DevExpress.XtraEditors.SimpleButton();
            this.BttIn = new DevExpress.XtraEditors.SimpleButton();
            this.BttXoa = new DevExpress.XtraEditors.SimpleButton();
            this.BttNapLai = new DevExpress.XtraEditors.SimpleButton();
            this.BttSua = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.GridCTChuyenTau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridCTChuyenTau
            // 
            this.GridCTChuyenTau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCTChuyenTau.Location = new System.Drawing.Point(0, 76);
            this.GridCTChuyenTau.MainView = this.gridView1;
            this.GridCTChuyenTau.Name = "GridCTChuyenTau";
            this.GridCTChuyenTau.Size = new System.Drawing.Size(878, 433);
            this.GridCTChuyenTau.TabIndex = 13;
            this.GridCTChuyenTau.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.GridCTChuyenTau.Click += new System.EventHandler(this.GridCTChuyenTau_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn4,
            this.gridColumn2,
            this.gridColumn5,
            this.gridColumn3});
            this.gridView1.GridControl = this.GridCTChuyenTau;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "STT";
            this.gridColumn1.FieldName = "STT";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Tuyến Đường";
            this.gridColumn4.FieldName = "TENTD";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Thời gian khởi hành";
            this.gridColumn2.DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
            this.gridColumn2.FieldName = "TGBD";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Thời gian đến";
            this.gridColumn5.DisplayFormat.FormatString = "MM/dd/yyyy hh:mm:ss";
            this.gridColumn5.FieldName = "TGKT";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.FieldName = "MATD";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.BttXuat);
            this.groupControl1.Controls.Add(this.BttThem);
            this.groupControl1.Controls.Add(this.BttIn);
            this.groupControl1.Controls.Add(this.BttXoa);
            this.groupControl1.Controls.Add(this.BttNapLai);
            this.groupControl1.Controls.Add(this.BttSua);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(878, 76);
            this.groupControl1.TabIndex = 12;
            // 
            // BttXuat
            // 
            this.BttXuat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttXuat.Image = ((System.Drawing.Image)(resources.GetObject("BttXuat.Image")));
            this.BttXuat.Location = new System.Drawing.Point(330, 24);
            this.BttXuat.Name = "BttXuat";
            this.BttXuat.Size = new System.Drawing.Size(41, 34);
            this.BttXuat.TabIndex = 5;
            this.BttXuat.Click += new System.EventHandler(this.BttXuat_Click);
            // 
            // BttThem
            // 
            this.BttThem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttThem.Image = ((System.Drawing.Image)(resources.GetObject("BttThem.Image")));
            this.BttThem.Location = new System.Drawing.Point(11, 24);
            this.BttThem.Name = "BttThem";
            this.BttThem.Size = new System.Drawing.Size(40, 34);
            this.BttThem.TabIndex = 4;
            this.BttThem.Click += new System.EventHandler(this.BttThem_Click);
            // 
            // BttIn
            // 
            this.BttIn.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttIn.Image = ((System.Drawing.Image)(resources.GetObject("BttIn.Image")));
            this.BttIn.Location = new System.Drawing.Point(263, 24);
            this.BttIn.Name = "BttIn";
            this.BttIn.Size = new System.Drawing.Size(39, 34);
            this.BttIn.TabIndex = 6;
            this.BttIn.Click += new System.EventHandler(this.BttIn_Click);
            // 
            // BttXoa
            // 
            this.BttXoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttXoa.Image = ((System.Drawing.Image)(resources.GetObject("BttXoa.Image")));
            this.BttXoa.Location = new System.Drawing.Point(72, 24);
            this.BttXoa.Name = "BttXoa";
            this.BttXoa.Size = new System.Drawing.Size(39, 34);
            this.BttXoa.TabIndex = 3;
            this.BttXoa.Click += new System.EventHandler(this.BttXoa_Click);
            // 
            // BttNapLai
            // 
            this.BttNapLai.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttNapLai.Image = ((System.Drawing.Image)(resources.GetObject("BttNapLai.Image")));
            this.BttNapLai.Location = new System.Drawing.Point(200, 24);
            this.BttNapLai.Name = "BttNapLai";
            this.BttNapLai.Size = new System.Drawing.Size(41, 34);
            this.BttNapLai.TabIndex = 7;
            this.BttNapLai.Click += new System.EventHandler(this.BttNapLai_Click);
            // 
            // BttSua
            // 
            this.BttSua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BttSua.Image = ((System.Drawing.Image)(resources.GetObject("BttSua.Image")));
            this.BttSua.Location = new System.Drawing.Point(130, 24);
            this.BttSua.Name = "BttSua";
            this.BttSua.Size = new System.Drawing.Size(41, 34);
            this.BttSua.TabIndex = 2;
            this.BttSua.Click += new System.EventHandler(this.BttSua_Click);
            // 
            // ChuyenTau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.GridCTChuyenTau);
            this.Controls.Add(this.groupControl1);
            this.Name = "ChuyenTau";
            this.Size = new System.Drawing.Size(878, 509);
            this.Load += new System.EventHandler(this.ChuyenTau_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridCTChuyenTau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl GridCTChuyenTau;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton BttXuat;
        private DevExpress.XtraEditors.SimpleButton BttThem;
        private DevExpress.XtraEditors.SimpleButton BttIn;
        private DevExpress.XtraEditors.SimpleButton BttXoa;
        private DevExpress.XtraEditors.SimpleButton BttNapLai;
        private DevExpress.XtraEditors.SimpleButton BttSua;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;


    }
}
