﻿using System;
using System.Data;
using System.Windows.Forms;
using QLDuongSat.Form.Danh_mục;
using DTO;
using BUS;

namespace QLDuongSat.UC_Danh_Muc
{
    public partial class ChuyenTau : UserControl
    {
        string MaCT;
        ChuyenTauDTO ct = new ChuyenTauDTO();  

        public ChuyenTau()
        {
            InitializeComponent();
        }

        private void ChuyenTau_Load(object sender, EventArgs e)
        {
            DataTable bang = new DataTable();
            bang = ChuyenTauBUS.LayBangChuyenTau();
            GridCTChuyenTau.DataSource = bang;
        }
        public void loadDS()
        {
            DataTable bang = new DataTable();
            bang = ChuyenTauBUS.LayBangChuyenTau();
            GridCTChuyenTau.DataSource = bang;
        }

        private void BttThem_Click(object sender, EventArgs e)
        {
            ChuyenTau_Them frm = new ChuyenTau_Them();
            frm.load = new ChuyenTau_Them.loadDS(loadDS);
            frm.ShowDialog();
        }

        private void BttNapLai_Click(object sender, EventArgs e)
        {
            ChuyenTau_Load(sender, e);
        }

        private void BttIn_Click(object sender, EventArgs e)
        {
            GridCTChuyenTau.ShowPrintPreview();
        }

        private void BttXuat_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "File Excel (*.Xls)|*.Xls";
            save.FileName = "Excel";
            save.ShowDialog();
            if (save.FileName != "")
            {
                GridCTChuyenTau.ExportToXls(save.FileName);
            }
        }

        private void BttSua_Click(object sender, EventArgs e)
        {
            ChuyenTau_Sua frm1 = new ChuyenTau_Sua();
            frm1.suaChuyen = ct;
            frm1.load = new ChuyenTau_Sua.loadDS(loadDS);
            frm1.ShowDialog();
        }

        private void GridCTChuyenTau_Click(object sender, EventArgs e)
        {
        
            ct.Thoigiankhoihanh = (DateTime.Parse(gridView1.GetFocusedRowCellValue(gridColumn2).ToString()));
            ct.Thoigianden = (DateTime.Parse(gridView1.GetFocusedRowCellValue(gridColumn5).ToString()));
            ct.Matuyen = (gridView1.GetFocusedRowCellValue(gridColumn3)).ToString();
            ct.STT = int.Parse((gridView1.GetFocusedRowCellValue(gridColumn1)).ToString());

        }

        private void BttXoa_Click(object sender, EventArgs e)
        {
            ChuyenTauBUS.XoaChuyenTau(ct);
            MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK);
            ChuyenTau_Load(sender, e);
        }



    }
}
