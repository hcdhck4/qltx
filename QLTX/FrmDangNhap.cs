﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DAO;
using System.IO;
using System.Linq;
using QLDuongSat.Form;
using DTO;
namespace QLDuongSat
{
    public partial class FrmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        public FrmDangNhap()
        {
            InitializeComponent();
        }
        public static string taikhoan;
        public static string matkhau;
        public static string Vaitro ;
        public static int Matk;
        public static  TaiKhoanDTO TK;
        //QLDUONGSATDataContext QLDS = new QLDUONGSATDataContext();
        int i = 0;
        private void FrmDangNhap_Load(object sender, EventArgs e)
        {         
            if (!File.Exists("Pass.txt"))//kiểm tra nếu chưa có file Pass.txt thì tạo ra file Pass.txt
            {
                FileStream fs;
                fs = new FileStream("Pass.txt", FileMode.Create);//Tạo file mới tên là Pass.txt
                StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);

                sWriter.WriteLine("Hello World!");
                sWriter.Flush();
                fs.Close();
            }
            String[] dong = File.ReadAllLines("Pass.txt");
            if (dong[dong.Length - 1] == "1")
            {
                TxtMatKhau.Text = dong[dong.Length - 3];
                TxtTenTaiKhoan.Text = dong[dong.Length - 2];
            }        
        }

        private void BttDangNhap_Click(object sender, EventArgs e)
        {
            //Kiểm tra tên đăng nhập có hợp lệ
            if (TxtTenTaiKhoan.Text == "")
            {
                dxErrorProvider1.SetError(TxtTenTaiKhoan, "Vui lòng không để trống");
                MessageBox.Show("Tên tài khoản không được để trống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SqlConnection cn;
            cn = DataProvider.Connection();

            string tsql = string.Format("select * from TAIKHOAN where Tennguoidung ='{0}' and Matkhau = '{1}'", TxtTenTaiKhoan.Text, TxtMatKhau.Text);
            SqlCommand cmd = new SqlCommand(tsql, cn);
            cmd.ExecuteScalar();

            SqlDataAdapter adt = new SqlDataAdapter(cmd);
            DataTable bang = new DataTable();
            adt.Fill(bang);
            //kiểm tra tài khoản có tồn tại
            if (bang.Rows.Count > 0)
            {
                //Gán vào 2 biến phục vụ cho việc đổi mật khẩu
                taikhoan = TxtTenTaiKhoan.Text;
                matkhau = TxtMatKhau.Text;          
                FileStream fs = new FileStream("Pass.txt", FileMode.Append);
                StreamWriter writeFile = new StreamWriter(fs, Encoding.UTF8);
                //Lấy vai trò để phân quyền xem màn hình 
               // var LayVaiTro = (from bangTK in QLDS.TAIKHOANs  where bangTK.Tennguoidung == taikhoan && bangTK.Matkhau == matkhau select bangTK).FirstOrDefault();
               // Vaitro = LayVaiTro.Mavaitro;
                //Matk = Int32.Parse(LayVaiTro.Matk);
                //              
                this.Hide();
                QLTX.FromMain frmmain = new QLTX.FromMain();
                frmmain.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Bạn nhập sai tên toài khoản hoặc mật khẩu!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BttHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtTenTaiKhoan_TextChanged(object sender, EventArgs e)
        {
            if (TxtTenTaiKhoan.Text == "")
            {
                dxErrorProvider1.SetError(TxtTenTaiKhoan, "Vui lòng không để trống");
            }
            else
                dxErrorProvider1.ClearErrors();
        }

        private void FrmDangNhap_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BttDangNhap_Click(sender, e);
            }
        }

    }
}