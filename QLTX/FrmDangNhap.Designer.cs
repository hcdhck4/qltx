﻿namespace QLDuongSat
{
    partial class FrmDangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDangNhap));
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.BttHuy = new DevExpress.XtraEditors.SimpleButton();
            this.BttDangNhap = new DevExpress.XtraEditors.SimpleButton();
            this.TxtTenTaiKhoan = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.PictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.TxtMatKhau = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenTaiKhoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMatKhau.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl1
            // 
            this.GroupControl1.Controls.Add(this.BttHuy);
            this.GroupControl1.Controls.Add(this.BttDangNhap);
            this.GroupControl1.Controls.Add(this.TxtTenTaiKhoan);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.PictureEdit1);
            this.GroupControl1.Controls.Add(this.TxtMatKhau);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupControl1.Location = new System.Drawing.Point(0, 0);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(526, 249);
            this.GroupControl1.TabIndex = 2;
            // 
            // BttHuy
            // 
            this.BttHuy.Image = ((System.Drawing.Image)(resources.GetObject("BttHuy.Image")));
            this.BttHuy.Location = new System.Drawing.Point(321, 157);
            this.BttHuy.Name = "BttHuy";
            this.BttHuy.Size = new System.Drawing.Size(108, 34);
            this.BttHuy.TabIndex = 4;
            this.BttHuy.Text = "Hủy";
            this.BttHuy.Click += new System.EventHandler(this.BttHuy_Click);
            // 
            // BttDangNhap
            // 
            this.BttDangNhap.Image = ((System.Drawing.Image)(resources.GetObject("BttDangNhap.Image")));
            this.BttDangNhap.Location = new System.Drawing.Point(183, 156);
            this.BttDangNhap.Name = "BttDangNhap";
            this.BttDangNhap.Size = new System.Drawing.Size(108, 34);
            this.BttDangNhap.TabIndex = 3;
            this.BttDangNhap.Text = "Đăng Nhập";
            this.BttDangNhap.Click += new System.EventHandler(this.BttDangNhap_Click);
            // 
            // TxtTenTaiKhoan
            // 
            this.TxtTenTaiKhoan.Location = new System.Drawing.Point(248, 89);
            this.TxtTenTaiKhoan.Name = "TxtTenTaiKhoan";
            this.TxtTenTaiKhoan.Size = new System.Drawing.Size(154, 20);
            this.TxtTenTaiKhoan.TabIndex = 0;
            this.TxtTenTaiKhoan.TextChanged += new System.EventHandler(this.TxtTenTaiKhoan_TextChanged);
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(162, 75);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(0, 13);
            this.LabelControl4.TabIndex = 9;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl3.Location = new System.Drawing.Point(202, 35);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(118, 23);
            this.LabelControl3.TabIndex = 5;
            this.LabelControl3.Text = "ĐĂNG NHẬP";
           
            // TxtMatKhau
            // 
            this.TxtMatKhau.Location = new System.Drawing.Point(248, 115);
            this.TxtMatKhau.Name = "TxtMatKhau";
            this.TxtMatKhau.Properties.PasswordChar = '*';
            this.TxtMatKhau.Size = new System.Drawing.Size(154, 20);
            this.TxtMatKhau.TabIndex = 1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl2.Location = new System.Drawing.Point(162, 117);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(73, 18);
            this.LabelControl2.TabIndex = 6;
            this.LabelControl2.Text = "Mật khẩu:";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl1.Location = new System.Drawing.Point(162, 89);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(78, 18);
            this.LabelControl1.TabIndex = 7;
            this.LabelControl1.Text = "Tài Khoản:";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // FrmDangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 249);
            this.Controls.Add(this.GroupControl1);
            this.Name = "FrmDangNhap";
            this.Text = "Đăng Nhập";
            this.Load += new System.EventHandler(this.FrmDangNhap_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmDangNhap_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenTaiKhoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMatKhau.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.SimpleButton BttHuy;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.SimpleButton BttDangNhap;
        internal DevExpress.XtraEditors.TextEdit TxtTenTaiKhoan;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.PictureEdit PictureEdit1;
        internal DevExpress.XtraEditors.TextEdit TxtMatKhau;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}