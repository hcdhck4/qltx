﻿namespace QLTX
{
    partial class FromMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
      
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {            
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::QLDuongSat.SplashScreen1), true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QLTX.FromMain));
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.appMenu = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.iBoldFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iItalicFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iUnderlinedFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iLeftTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iCenterTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iRightTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.BttHuongDan = new DevExpress.XtraBars.BarButtonItem();
            this.BttTau = new DevExpress.XtraBars.BarButtonItem();
            this.BttVe = new DevExpress.XtraBars.BarButtonItem();
            this.BttBoPhan = new DevExpress.XtraBars.BarButtonItem();
            this.BttNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.BttBanve = new DevExpress.XtraBars.BarButtonItem();
            this.BttThongKe = new DevExpress.XtraBars.BarButtonItem();
            this.BttDonViTinh = new DevExpress.XtraBars.BarButtonItem();
            this.BttTuyen = new DevExpress.XtraBars.BarButtonItem();
            this.BttChuyenTau = new DevExpress.XtraBars.BarButtonItem();
            this.BttHanhTrinh = new DevExpress.XtraBars.BarButtonItem();
            this.BttHanhKhach = new DevExpress.XtraBars.BarButtonItem();
            this.BttPhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.BttDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.BttSaoLuu = new DevExpress.XtraBars.BarButtonItem();
            this.BttGa = new DevExpress.XtraBars.BarButtonItem();
            this.BttPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.BttBanVeOL = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.homeRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.formatRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.skinsRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RipDanhMuc = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RipChucNang = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.appMenu;
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Images = this.ribbonImageCollection;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.iExit,
            this.iBoldFontStyle,
            this.iItalicFontStyle,
            this.iUnderlinedFontStyle,
            this.iLeftTextAlign,
            this.iCenterTextAlign,
            this.iRightTextAlign,
            this.rgbiSkins,
            this.BttHuongDan,
            this.BttTau,
            this.BttVe,
            this.BttBoPhan,
            this.BttNhanVien,
            this.BttBanve,
            this.BttThongKe,
            this.BttDonViTinh,
            this.BttTuyen,
            this.BttChuyenTau,
            this.BttHanhTrinh,
            this.BttHanhKhach,
            this.BttPhanQuyen,
            this.BttDoiMatKhau,
            this.BttSaoLuu,
            this.BttGa,
            this.BttPhucHoi,
            this.BttBanVeOL});
            this.ribbonControl.LargeImages = this.ribbonImageCollectionLarge;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 93;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.homeRibbonPage,
            this.RipDanhMuc,
            this.RipChucNang,
            this.ribbonPage2});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl.Size = new System.Drawing.Size(1024, 162);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            // 
            // appMenu
            // 
            this.appMenu.ItemLinks.Add(this.iExit);
            this.appMenu.Name = "appMenu";
            this.appMenu.Ribbon = this.ribbonControl;
            this.appMenu.ShowRightPane = true;
            // 
            // iExit
            // 
            this.iExit.Caption = "Exit";
            this.iExit.Description = "Closes this program after prompting you to save unsaved data.";
            this.iExit.Hint = "Closes this program after prompting you to save unsaved data";
            this.iExit.Id = 20;
            this.iExit.ImageIndex = 6;
            this.iExit.LargeImageIndex = 6;
            this.iExit.Name = "iExit";
            this.iExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExit_ItemClick);
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            // 
            // iBoldFontStyle
            // 
            this.iBoldFontStyle.Caption = "Bold";
            this.iBoldFontStyle.Id = 53;
            this.iBoldFontStyle.ImageIndex = 9;
            this.iBoldFontStyle.Name = "iBoldFontStyle";
            // 
            // iItalicFontStyle
            // 
            this.iItalicFontStyle.Caption = "Italic";
            this.iItalicFontStyle.Id = 54;
            this.iItalicFontStyle.ImageIndex = 10;
            this.iItalicFontStyle.Name = "iItalicFontStyle";
            // 
            // iUnderlinedFontStyle
            // 
            this.iUnderlinedFontStyle.Caption = "Underlined";
            this.iUnderlinedFontStyle.Id = 55;
            this.iUnderlinedFontStyle.ImageIndex = 11;
            this.iUnderlinedFontStyle.Name = "iUnderlinedFontStyle";
            // 
            // iLeftTextAlign
            // 
            this.iLeftTextAlign.Caption = "Left";
            this.iLeftTextAlign.Id = 57;
            this.iLeftTextAlign.ImageIndex = 12;
            this.iLeftTextAlign.Name = "iLeftTextAlign";
            // 
            // iCenterTextAlign
            // 
            this.iCenterTextAlign.Caption = "Center";
            this.iCenterTextAlign.Id = 58;
            this.iCenterTextAlign.ImageIndex = 13;
            this.iCenterTextAlign.Name = "iCenterTextAlign";
            // 
            // iRightTextAlign
            // 
            this.iRightTextAlign.Caption = "Right";
            this.iRightTextAlign.Id = 59;
            this.iRightTextAlign.ImageIndex = 14;
            this.iRightTextAlign.Name = "iRightTextAlign";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Skins";
            // 
            // 
            // 
            this.rgbiSkins.Gallery.AllowHoverImages = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.ColumnCount = 4;
            this.rgbiSkins.Gallery.FixedHoverImageSize = false;
            this.rgbiSkins.Gallery.ImageSize = new System.Drawing.Size(32, 17);
            this.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.rgbiSkins.Gallery.RowCount = 4;
            this.rgbiSkins.Id = 60;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // BttHuongDan
            // 
            this.BttHuongDan.Caption = "Hướng Dẫn";
            this.BttHuongDan.Glyph = ((System.Drawing.Image)(resources.GetObject("BttHuongDan.Glyph")));
            this.BttHuongDan.Id = 62;
            this.BttHuongDan.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BttHuongDan.LargeGlyph")));
            this.BttHuongDan.LargeWidth = 70;
            this.BttHuongDan.Name = "BttHuongDan";
            this.BttHuongDan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttHuongDan_ItemClick);
            // 
            // BttTau
            // 
            this.BttTau.Id = 83;
            this.BttTau.Name = "BttTau";
            // 
            // BttVe
            // 
            this.BttVe.Id = 85;
            this.BttVe.Name = "BttVe";
            // 
            // BttBoPhan
            // 
            this.BttBoPhan.Id = 86;
            this.BttBoPhan.Name = "BttBoPhan";
            // 
            // BttNhanVien
            // 
            this.BttNhanVien.Id = 87;
            this.BttNhanVien.Name = "BttNhanVien";
            // 
            // BttBanve
            // 
            this.BttBanve.Id = 88;
            this.BttBanve.Name = "BttBanve";
            // 
            // BttTuyen
            // 
            this.BttTuyen.Caption = "Tuyến";
            this.BttTuyen.Id = 71;
            this.BttTuyen.LargeGlyph = global::QLTX.Properties.Resources.Tuyen;
            this.BttTuyen.LargeWidth = 70;
            this.BttTuyen.Name = "BttTuyen";
            this.BttTuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttTuyen_ItemClick);
            // 
            // BttChuyenTau
            // 
            this.BttChuyenTau.Caption = "Chuyến Tàu";
            this.BttChuyenTau.Id = 72;
            this.BttChuyenTau.LargeGlyph = global::QLTX.Properties.Resources.Chuyen_tau;
            this.BttChuyenTau.LargeWidth = 70;
            this.BttChuyenTau.Name = "BttChuyenTau";
            this.BttChuyenTau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttChuyenTau_ItemClick);
            // 
            // BttHanhTrinh
            // 
            this.BttHanhTrinh.Caption = "Hành Trình";
            this.BttHanhTrinh.Id = 73;
            this.BttHanhTrinh.LargeGlyph = global::QLTX.Properties.Resources.Hanh_Trinh;
            this.BttHanhTrinh.LargeWidth = 70;
            this.BttHanhTrinh.Name = "BttHanhTrinh";
            this.BttHanhTrinh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttHanhTrinh_ItemClick);
            // 
            // BttHanhKhach
            // 
            this.BttHanhKhach.Id = 90;
            this.BttHanhKhach.Name = "BttHanhKhach";
            // 
            // BttPhanQuyen
            // 
            this.BttPhanQuyen.Caption = "Phân Quyền";
            this.BttPhanQuyen.Id = 75;
            this.BttPhanQuyen.LargeWidth = 70;
            this.BttPhanQuyen.Name = "BttPhanQuyen";
            this.BttPhanQuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttPhanQuyen_ItemClick);
            // 
            // BttDoiMatKhau
            // 
            this.BttDoiMatKhau.Caption = "Đổi Mật Khẩu";
            this.BttDoiMatKhau.Id = 76;
            this.BttDoiMatKhau.LargeGlyph = global::QLTX.Properties.Resources.key;
            this.BttDoiMatKhau.LargeWidth = 70;
            this.BttDoiMatKhau.Name = "BttDoiMatKhau";
            this.BttDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttDoiMatKhau_ItemClick);
            // 
            // BttSaoLuu
            // 
            this.BttSaoLuu.Caption = "Sao Lưu";
            this.BttSaoLuu.Glyph = ((System.Drawing.Image)(resources.GetObject("BttSaoLuu.Glyph")));
            this.BttSaoLuu.Id = 78;
            this.BttSaoLuu.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BttSaoLuu.LargeGlyph")));
            this.BttSaoLuu.Name = "BttSaoLuu";
            this.BttSaoLuu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttSaoLuu_ItemClick);
            // 
            // BttGa
            // 
            this.BttGa.Id = 91;
            this.BttGa.Name = "BttGa";
            // 
            // BttPhucHoi
            // 
            this.BttPhucHoi.Caption = "Phục Hồi";
            this.BttPhucHoi.Glyph = ((System.Drawing.Image)(resources.GetObject("BttPhucHoi.Glyph")));
            this.BttPhucHoi.Id = 81;
            this.BttPhucHoi.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BttPhucHoi.LargeGlyph")));
            this.BttPhucHoi.Name = "BttPhucHoi";
            this.BttPhucHoi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BttPhucHoi_ItemClick);
            // 
            // BttBanVeOL
            // 
            this.BttBanVeOL.Id = 92;
            this.BttBanVeOL.Name = "BttBanVeOL";
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            // 
            // homeRibbonPage
            // 
            this.homeRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.formatRibbonPageGroup,
            this.skinsRibbonPageGroup});
            this.homeRibbonPage.Image = ((System.Drawing.Image)(resources.GetObject("homeRibbonPage.Image")));
            this.homeRibbonPage.Name = "homeRibbonPage";
            this.homeRibbonPage.Text = "Hệ Thống";
            // 
            // formatRibbonPageGroup
            // 
            this.formatRibbonPageGroup.ItemLinks.Add(this.BttSaoLuu);
            this.formatRibbonPageGroup.ItemLinks.Add(this.BttPhucHoi);
            this.formatRibbonPageGroup.Name = "formatRibbonPageGroup";
            this.formatRibbonPageGroup.Text = "Dữ Liệu";
            // 
            // skinsRibbonPageGroup
            // 
            this.skinsRibbonPageGroup.ItemLinks.Add(this.rgbiSkins);
            this.skinsRibbonPageGroup.Name = "skinsRibbonPageGroup";
            this.skinsRibbonPageGroup.ShowCaptionButton = false;
            this.skinsRibbonPageGroup.Text = "Giao Diện";
            // 
            // RipDanhMuc
            // 
            this.RipDanhMuc.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup9});
            this.RipDanhMuc.Image = ((System.Drawing.Image)(resources.GetObject("RipDanhMuc.Image")));
            this.RipDanhMuc.Name = "RipDanhMuc";
            this.RipDanhMuc.Text = "Danh Mục";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.BttTuyen);
            this.ribbonPageGroup9.ItemLinks.Add(this.BttChuyenTau);
            this.ribbonPageGroup9.ItemLinks.Add(this.BttHanhTrinh);
            this.ribbonPageGroup9.ItemLinks.Add(this.BttGa);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Hành Trình";
            // 
            // RipChucNang
            // 
            this.RipChucNang.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6});
            this.RipChucNang.Image = ((System.Drawing.Image)(resources.GetObject("RipChucNang.Image")));
            this.RipChucNang.Name = "RipChucNang";
            this.RipChucNang.Text = "Chức Năng";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.BttThongKe);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Báo Cáo";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7});
            this.ribbonPage2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage2.Image")));
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Trợ Giúp";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.BttHuongDan);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Trợ Giúp";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 405);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1024, 31);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 162);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.Size = new System.Drawing.Size(1024, 243);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.CloseButtonClick += new System.EventHandler(this.xtraTabControl1_CloseButtonClick);
            this.xtraTabControl1.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.xtraTabControl1_ControlAdded);
            // 
            // FromMain
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 436);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.Name = "FromMain";
            this.Ribbon = this.ribbonControl;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Quản Lý Tài Xế";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FromMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarButtonItem iBoldFontStyle;
        private DevExpress.XtraBars.BarButtonItem iItalicFontStyle;
        private DevExpress.XtraBars.BarButtonItem iUnderlinedFontStyle;
        private DevExpress.XtraBars.BarButtonItem iLeftTextAlign;
        private DevExpress.XtraBars.BarButtonItem iCenterTextAlign;
        private DevExpress.XtraBars.BarButtonItem iRightTextAlign;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.Ribbon.RibbonPage homeRibbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup formatRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup skinsRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu appMenu;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.Utils.ImageCollection ribbonImageCollection;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private DevExpress.XtraBars.BarButtonItem BttHuongDan;
        private DevExpress.XtraBars.Ribbon.RibbonPage RipDanhMuc;
        private DevExpress.XtraBars.Ribbon.RibbonPage RipChucNang;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem BttTau;
        private DevExpress.XtraBars.BarButtonItem BttVe;
        private DevExpress.XtraBars.BarButtonItem BttBoPhan;
        private DevExpress.XtraBars.BarButtonItem BttNhanVien;
        private DevExpress.XtraBars.BarButtonItem BttBanve;
        private DevExpress.XtraBars.BarButtonItem BttThongKe;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraBars.BarButtonItem BttDonViTinh;
        private DevExpress.XtraBars.BarButtonItem BttTuyen;
        private DevExpress.XtraBars.BarButtonItem BttChuyenTau;
        private DevExpress.XtraBars.BarButtonItem BttHanhTrinh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarButtonItem BttHanhKhach;
        private DevExpress.XtraBars.BarButtonItem BttPhanQuyen;
        private DevExpress.XtraBars.BarButtonItem BttDoiMatKhau;
        private DevExpress.XtraBars.BarButtonItem BttSaoLuu;
        private DevExpress.XtraBars.BarButtonItem BttGa;
        private DevExpress.XtraBars.BarButtonItem BttPhucHoi;
        private DevExpress.XtraBars.BarButtonItem BttBanVeOL;
        private System.Windows.Forms.Timer timer1;

    }
}
