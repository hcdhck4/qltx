USE [master]
GO
/****** Object:  Database [QLTX]    Script Date: 8/29/2017 12:48:56 AM ******/
CREATE DATABASE [QLTX] ON  PRIMARY 
( NAME = N'QLTX', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\QLTX.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLTX_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\QLTX_log.LDF' , SIZE = 504KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLTX] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLTX].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLTX] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLTX] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLTX] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLTX] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLTX] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLTX] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLTX] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QLTX] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLTX] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLTX] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLTX] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLTX] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLTX] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLTX] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLTX] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLTX] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QLTX] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLTX] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLTX] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLTX] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLTX] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLTX] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLTX] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLTX] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLTX] SET  MULTI_USER 
GO
ALTER DATABASE [QLTX] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLTX] SET DB_CHAINING OFF 
GO
USE [QLTX]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateChuyenXe]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_CreateChuyenXe]
	@Matd varchar(50),
	@Stt int,
	@TGBD datetime,
	@TGKT datetime
AS
BEGIN
	SET NOCOUNT ON;
	Declare @checkMatd varchar(50)
	Declare @checkStt int
	Declare @checkTGBD datetime
	Declare @checkTGKT datetime

	set @checkMatd = @Matd
	set @checkStt = @checkStt
	set @checkTGBD = @TGBD
	set @checkTGKT = @TGKT
	IF @checkMatd IS NULL
		BEGIN
			PRINT 'khong co ma tuyen duong'
			RETURN 0
		END
	IF @checkStt IS NULL		
		BEGIN
			PRINT 'khong co stt'
			RETURN 0
		END
	IF @checkTGBD IS NULL		
		BEGIN
			PRINT 'khong co thoi gian bat dau'
			RETURN 0
		END
	IF @checkTGKT IS NULL		
		BEGIN
			PRINT 'khong co thoi gian ket thuc'
			RETURN 0
		END
	Declare @checkCX varchar(50)
		Set @checkCX = (Select CX.MATD
						From CHUYENXE CX
						Where CX.MATD = @Matd and CX.STT = @Stt  )

	IF @checkCX IS NOT NULL
		BEGIN
			PRINT 'Da ton tai'
			RETURN 0
		END

	insert into CHUYENXE 
		values (@Matd,@Stt,@TGBD,@TGKT)
	PRINT 'Them Lich Trinh'
	RETURN 1
END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateLichTrinh]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateLichTrinh]
	@Maus varchar(50),
	@Matd varchar(50),
	@Stt int,
	@Bienxe varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @checkMaus varchar(50)
	Declare @checkMatd varchar(50)
	Declare @checkStt int
	set @checkMaus = @Maus
	set @checkMatd = @Matd
	set @checkStt = @checkStt
	IF @checkMaus IS NULL
		BEGIN
			PRINT 'khong co ma user'
			RETURN 0
		END
	IF @checkMatd IS NULL
		BEGIN
			PRINT 'khong co ma tuyen duong'
			RETURN 0
		END
	IF @checkStt IS NULL		
		BEGIN
			PRINT 'khong co stt'
			RETURN 0
		END
	Declare @checkLT varchar(50)
		Set @checkLT = (Select LT.MAUS
						From LICHTRINH LT
						Where LT.MAUS = @Maus and LT.MATD = @Matd and LT.STT = @Stt)

	IF @checkLT IS NOT NULL
		BEGIN
			PRINT 'Da ton tai'
			RETURN 0
		END

	insert into LICHTRINH 
		values (@Maus,@Matd,@Stt,@Bienxe)
	PRINT 'Them Lich Trinh'
	RETURN 1
END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateTaiKhoan]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_CreateTaiKhoan]
	@maUs nvarchar(50),
	@tenUs nvarchar(50),
	@taiKhoan nvarchar(50),
	@matKhau nvarchar(50),
	@dienThoai nvarchar(50),
	@gioiTinh bit,
	@khaNang float,
	@maQuyen nvarchar(50),
	@maTo nvarchar(50),
	@txql nvarchar(50)
AS
	BEGIN
		declare @flag bit = 1;
		--1 validate empty
		if (@tenUs = '') begin
			PRINT 'tenUser required'
			set @flag = 0
		end
		if (@taiKhoan = '') begin
			PRINT '@taiKhoan required'
			set @flag = 0
		end
		if (@matKhau = '') begin
			PRINT '@matKhau required'
			set @flag = 0
		end
		if (@dienThoai = '') begin
			PRINT '@dienThoai required'
			set @flag = 0
		end
		if (@khaNang = '') begin
			PRINT '@khaNang required or not number'
			set @flag = 0
		end
		if (@gioiTinh = '') begin
			PRINT '@gioiTinh required'
			set @flag = 0
		end
		if (@txql is not null) begin
			PRINT '@tai xe quan ly xe required'
			set @flag = 0
		end

		--2 CHECK DB
		-- 2.1 CHECK TAI XE QUAN LY
		DECLARE @CHECKTXQL VARCHAR(50)
		SET @CHECKTXQL = (SELECT TOP 1 U.MAUS
							FROM NGUOIDUNG U
							WHERE U.TXQL = @txql)

			if (@CHECKTXQL = null) BEGIN
				PRINT '@txql DONT EXIST IN DATABASE'
				set @flag = 0
			END
			-- 2.2 CHECK QUYEN
			DECLARE @CHECKQUYEN VARCHAR(50)
			SET @CHECKQUYEN = (SELECT TOP 1 U.MAQUYEN
							FROM QUYEN U
							WHERE U.MAQUYEN = @maQuyen)

			if (@CHECKQUYEN IS null) BEGIN
					PRINT '@MAQUYEN foreign OF USER DONT EXIST IN DATABASE'
					set @flag = 0
			END
			-- end 2.2 CHECK QUYEN
			-- 2.3 CHECK mato
			DECLARE @CHECKMATO VARCHAR(50)
			SET @CHECKMATO = (SELECT TOP 1 U.MATO
							FROM [TO] U
							WHERE U.MATO = @maTo)

			if (@CHECKMATO IS null) BEGIN
					PRINT '@maTo foreign OF USER DONT EXIST IN DATABASE'
					set @flag = 0
			END
			-- end 2.3 CHECK mato
			

		print @CHECKQUYEN
		IF(@flag = 0) begin
			return 0
		end
		insert into NGUOIDUNG(MAUS, TENUS, TAIKHOAN, MATKHAU, DIENTHOAI, GIOITINH, KHANANG, MAQUYEN, MATO, TXQL)
		values(@maUs, @tenUs, @taiKhoan, @matKhau, @dienThoai, @gioiTinh, @khaNang, @maQuyen, @maTo, @txql)
		return 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateTo]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_CreateTo]
	@maTo varchar(50),
	@tenTo nvarchar(50),
	@toTruong varchar(50)
AS
	BEGIN
		Declare @checkTo varchar(50)
		Set @checkTo = (Select TX.MATO
						From TOXE TX
						Where TX.MATO = @maTo)
		-- Buoc 1: Kiem tra to ton tai chua
		IF @checkTo IS NOT NULL
			BEGIN
				PRINT 'TB01: To ' + @maTo + ' da ton tai'
				RETURN 0
			END
		-- Buoc 2: Kiem tra To truong ton tai chua
		IF @toTruong IS NOT NULL 
			BEGIN
				Declare @checkTT varchar(50)
				Set @checkTT = (Select ND.MAUS
							From NGUOIDUNG ND
							Where ND.MAUS = @toTruong)
				IF @checkTT IS  NULL 
					BEGIN
						PRINT 'TB02: To truong ' + @toTruong + ' khong ton tai'
						RETURN 0
					END
			END
			
		-- Buoc 2: Them to xe vao DB
		INSERT INTO TOXE(MATO, TENTO, TOTRUONG) VALUES (@maTo, @tenTo, @toTruong)
		PRINT 'Them to xe thanh cong'
		RETURN 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateTuyenDuong]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_CreateTuyenDuong]
	@maTD varchar(50),
	@tenTD nvarchar(50),
	@chieuDai float
AS
	BEGIN
		Declare @checkTD varchar(50)
		Set @checkTD = (Select TD.MATD
						From TUYENDUONG TD
						Where TD.MATD = @maTD)
		-- Buoc 1: Kiem tra tuyen duong ton tai chua
		IF @checkTD IS NOT NULL
			BEGIN
				PRINT 'TB01: ' + @maTD + ' da ton tai'
				RETURN 0
			END
		-- Buoc 2: Them tuyen duong vao DB
		INSERT INTO TUYENDUONG(MATD, TENTD, CHIEUDAI) VALUES (@maTD, @tenTD, @chieuDai)
		PRINT 'Them tuyen duong thanh cong'
		RETURN 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_DangNhap]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_DangNhap]
	@taiKhoan nvarchar(50),
	@matKhau nvarchar(50)
AS
	BEGIN
		declare @flag bit = 1
		--1 validate empty
		if (@taiKhoan = '') begin
			PRINT '@taiKhoan required'
			set @flag = 0
		end
		if (@matKhau = '') begin
			PRINT '@matKhau required'
			set @flag = 0
		end
		-- check
		DECLARE @check VARCHAR(50)
		SET @check = (SELECT TOP 1 U.MAUS
							FROM NGUOIDUNG U
							WHERE U.MATKHAU  = @matKhau
							and u.TAIKHOAN = @taiKhoan)

			if (@check is null) BEGIN
				PRINT '@username or password wrong'
				set @flag = 1
			END	
			return @flag
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteChuyenXe]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_DeleteChuyenXe]
	@maTD VARCHAR(50),
	@sTT int
AS
	BEGIN
		-- Buoc 1: Kiem tra chuyen xe da duoc phan cong vao lich trinh chua
		DECLARE @checkCX_LT VARCHAR(50)
		SET @checkCX_LT = (SELECT TOP 1 LT.MATD
							FROM LICHTRINH LT
							WHERE LT.MATD = @maTD AND LT.STT = @sTT)
		IF @checkCX_LT IS NOT NULL
			BEGIN
				PRINT 'TB01: Chuyen xe (' + @maTD + ', ' + CONVERT(varchar(10), @sTT) + ') da duoc phan vao lich trinh, vui long xoa lich trinh truoc';
				RETURN 0
			END
		
		-- Buoc 2: Kiem tra chuyen xe co ton tai khong
		DECLARE @checkCX VARCHAR(50)
		SET @checkCX = (SELECT TOP 1 CX.MATD
							FROM CHUYENXE CX
							WHERE CX.MATD = @maTD AND CX.STT = @sTT)
		IF @checkCX IS NULL
			BEGIN
				PRINT 'TB02: Chuyen xe (' + @maTD + ', ' + CONVERT(varchar(10), @sTT) +') khong ton tai';
				RETURN 0
			END
			
		-- Buoc 3: Xoa chuyen xe
		DELETE FROM CHUYENXE WHERE MATD = @maTD AND STT = @sTT
		PRINT 'Xoa chuyen xe hoan tat'
		RETURN 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteLichTrinh]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_DeleteLichTrinh]
	@Maus varchar(50),
	@Matd varchar(50),
	@Stt int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @checkLT VARCHAR(50)
	SET @checkLT = (SELECT LT.MAUS
						FROM LICHTRINH LT
						WHERE LT.MAUS = @Maus and LT.MATD = @Matd and LT.STT = @Stt)
	IF @checkLT IS NULL
		BEGIN
			PRINT 'Lich Trinh khong ton tai';
			RETURN 0
		END 
	DELETE FROM LICHTRINH WHERE MAUS = @Maus and MATD = @Matd and STT = @Stt
	PRINT 'Xoa lich trinh hoan tat'
	RETURN 1
END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTaiKhoan]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_DeleteTaiKhoan]
	@maUs nvarchar(50)
AS
	BEGIN
		declare @flag bit = 1;
		--1 validate empty
		if (@maUs = '' or @maUs is null) begin
			PRINT 'tenUser required'
			set @flag = 0
		end
	
	-- 2. check user exist
	DECLARE @check VARCHAR(50)
		SET @check = (SELECT TOP 1 U.MAUS
							FROM NGUOIDUNG U
							WHERE U.MAUS = @maUs)

			if (@check is null) BEGIN
				PRINT '@maUs DONT EXIST IN DATABASE'
				set @flag = 0
			END
	-- END 2. check user exist
		
		DELETE FROM NGUOIDUNG WHERE MAUS = @maUs
		return 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTo]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_DeleteTo]
	@Mato varchar(50)
AS
BEGIN

		DECLARE @checkTo VARCHAR(50)
		SET @checkTo = (SELECT T.MATO
							FROM TOXE T
							WHERE T.MATO = @Mato)
		IF @checkTo IS NULL
			BEGIN
				PRINT 'To khong ton tai';
				RETURN 0
			END
			
		DELETE FROM TOXE WHERE MATO = @Mato
		PRINT 'Xoa to hoan tat'
		RETURN 1
	
END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTuyenDuong]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_DeleteTuyenDuong]
	@maTD VARCHAR(50)
AS
	BEGIN
		-- Buoc 1: Kiem tra chuyen xe da duoc phan cong vao lich trinh chua
		DECLARE @checkCX VARCHAR(50)
		SET @checkCX = (SELECT TOP 1 CX.MATD
							FROM CHUYENXE CX
							WHERE CX.MATD = @maTD)
		IF @checkCX IS NOT NULL
			BEGIN
				PRINT 'TB01: Tuyen duong (' + @maTD + ') da duoc phan vao chuyen xe, vui long xoa chuyen xe truoc';
				RETURN 0
			END
		
		-- Buoc 2: Kiem tra chuyen xe co ton tai khong
		DECLARE @checkTD VARCHAR(50)
		SET @checkTD = (SELECT TD.MATD
							FROM TUYENDUONG TD
							WHERE TD.MATD = @maTD)
		IF @checkTD IS NULL
			BEGIN
				PRINT 'TB02: Tuyen duong (' + @maTD + ') khong ton tai';
				RETURN 0
			END
			
		-- Buoc 3: Xoa chuyen xe
		DELETE FROM TUYENDUONG WHERE MATD = @maTD
		PRINT 'Xoa tuyen duong hoan tat'
		RETURN 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetTuyenDuong]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetTuyenDuong] 
AS
BEGIN
	SELECT * from TUYENDUONG
END

GO
/****** Object:  StoredProcedure [dbo].[sp_SearchChuyenXe]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_SearchChuyenXe]
	@maTD VARCHAR(50),
	@sTT int
AS
	BEGIN
		IF (@maTD IS NOT NULL AND @sTT IS NOT NULL)
			BEGIN
				SELECT *
				FROM CHUYENXE CX
				WHERE CX.MATD = @maTD AND CX.STT = @sTT
			END
		ELSE
			IF @maTD IS  NULL
				BEGIN
					SELECT *
					FROM CHUYENXE CX
					WHERE CX.STT = @sTT
				END
			ELSE 
				BEGIN
					SELECT *
					FROM CHUYENXE CX
					WHERE CX.MATD = @maTD
				END
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchLichTrinh]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SearchLichTrinh]
	@Maus varchar(50),
	@Matd varchar(50),
	@Stt int
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT * FROM LICHTRINH WHERE MAUS LIKE @Maus or MATD LIKE @Matd or STT LIKE @Stt
END


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchTaiKhoan]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_SearchTaiKhoan]
	@q nvarchar(50)
AS
	BEGIN
		select * from NGUOIDUNG u
		where u.DIENTHOAI like '%' +  @q + '%'
		or u.TENUS  like '%' +  @q + '%'
		or u.TAIKHOAN like   '%' +  @q + '%'
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchTo]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SearchTo] 
	@Mato varchar(50)
AS
BEGIN
	SELECT * FROM TOXE WHERE MATO LIKE @Mato
END


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchTuyenDuong]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_SearchTuyenDuong]
	@maTD VARCHAR(50)
AS
	BEGIN
		Declare @checkTD varchar(50)
		SET @checkTD = (SELECT TOP 1 TD.MATD
						From TUYENDUONG TD
						Where TD.MATD = @maTD)
		IF @checkTD IS  NULL
			BEGIN
				PRINT 'TB01: Khong co tuyen duong ' + @maTD
				RETURN 0
			END
		ELSE 
			BEGIN
				SELECT *
				From TUYENDUONG TD
				Where TD.MATD = @maTD
				RETURN 1
			END
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateChuyenXe]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_UpdateChuyenXe]
	@matd nvarchar(50),
	@stt integer,
	@tgbd datetime,
	@tgkt datetime

AS
	BEGIN
	update CHUYENXE
	set TGBD = @tgbd,
	TGKT = @tgkt
	where
		MATD = @matd
		and STT  = @stt
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateLichTrinh]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateLichTrinh]
	@Maus varchar(50),
	@Matd varchar(50),
	@Stt int,
	@Bienxe varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @checkMaus varchar(50)
	Declare @checkMatd varchar(50)
	Declare @checkStt int
	set @checkMaus = @Maus
	set @checkMatd = @Matd
	set @checkStt = @Stt
	IF @checkMaus IS NULL
		BEGIN
			PRINT 'khong co ma user'
			RETURN 0
		END
	IF @checkMatd IS NULL
		BEGIN
			PRINT 'khong co ma tuyen duong'
			RETURN 0
		END
	IF @checkStt IS NULL		
		BEGIN
			PRINT 'khong co stt'
			RETURN 0
		END
	DECLARE @checkLT VARCHAR(50)
	SET @checkLT = (SELECT LT.MAUS
						FROM LICHTRINH LT
						WHERE LT.MAUS = @Maus and LT.MATD = @Matd and LT.STT = @Stt)
	IF @checkLT IS NULL
		BEGIN
			PRINT 'Lich Trinh khong ton tai';
			RETURN 0
		END 

	UPDATE LICHTRINH
	SET BIENXE = @Bienxe
	WHERE MAUS = @Maus and MATD = @Matd and STT = @Stt
	PRINT 'cap nhat lich trinh thanh cong'
	RETURN 1
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateThongTin]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_UpdateThongTin]
	@tenUs nvarchar(50),
	@taiKhoan nvarchar(50),
	@matKhau nvarchar(50),
	@dienThoai nvarchar(50),
	@gioiTinh bit,
	@khaNang float,
	@maQuyen nvarchar(50),
	@maTo nvarchar(50),
	@txql nvarchar(50)
AS
	BEGIN
		
		--1 validate empty
		if (@tenUs = '') begin
			PRINT 'tenUser required'
		end
		if (@taiKhoan = '') begin
			PRINT '@taiKhoan required'
		end
		if (@matKhau = '') begin
			PRINT '@matKhau required'
		end
		if (@dienThoai = '') begin
			PRINT '@dienThoai required'
		end
		if (@khaNang = '') begin
			PRINT '@khaNang required or not number'
		end
		if (@gioiTinh = '') begin
			PRINT '@gioiTinh required'
		end
			
		Update NGUOIDUNG 
		set  
		 NGUOIDUNG.TENUS = 	@tenUs ,
			NGUOIDUNG.TAIKHOAN  = @taiKhoan ,
			NGUOIDUNG.MATKHAU  = @matKhau ,
			NGUOIDUNG.DIENTHOAI= @dienThoai ,
			NGUOIDUNG.GIOITINH= @gioiTinh,
			NGUOIDUNG.KHANANG  = @khaNang,
			NGUOIDUNG.MAQUYEN  = @maQuyen ,
			NGUOIDUNG.MATO  = @maTo ,
			NGUOIDUNG.TXQL= @txql 
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateTo]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_UpdateTo]
	@maTo varchar(50),
	@tenTo nvarchar(50),
	@toTruong varchar(50)
AS
	BEGIN
		Declare @checkTo varchar(50)
		Set @checkTo = (Select TX.MATO
						From TOXE TX
						Where TX.MATO = @maTo)
		-- Buoc 1: Kiem tra to ton tai chua
		IF @checkTo IS NULL
			BEGIN
				PRINT 'TB01: To ' + @maTo + ' khong ton tai'
				RETURN 0
			END
		-- Buoc 2: Kiem tra To truong ton tai chua
		IF @toTruong IS NOT NULL 
			BEGIN
				Declare @checkTT varchar(50)
				Set @checkTT = (Select ND.MAUS
							From NGUOIDUNG ND
							Where ND.MAUS = @toTruong)
				IF @checkTT IS  NULL 
					BEGIN
						PRINT 'TB02: To truong ' + @toTruong + ' khong ton tai'
						RETURN 0
					END
			END
			
		-- Buoc 3: Cap nhat to xe vao DB
		UPDATE TOXE SET MATO = @maTo, TOTRUONG = @toTruong  WHERE MATO = @maTo
		PRINT 'Cap nhat to xe thanh cong'
		RETURN 1
	END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateTuyenDuong]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[sp_UpdateTuyenDuong]
	@maTD varchar(50),
	@tenTD nvarchar(50),
	@chieuDai float
AS
	BEGIN
		Declare @checkTD varchar(50)
		Set @checkTD = (Select TD.MATD
						From TUYENDUONG TD
						Where TD.MATD = @maTD)
		-- Buoc 1: Kiem tra tuyen duong ton tai chua
		IF @checkTD IS NULL
			BEGIN
				PRINT 'TB01: ' + @maTD + ' khong ton tai'
				RETURN 0
			END
		-- Buoc 2: Cap nhat tuyen duong vao DB
		UPDATE TUYENDUONG SET TENTD = @tenTD, CHIEUDAI = @chieuDai WHERE MATD = @maTD
		PRINT 'Cap nhat tuyen duong thanh cong'
		RETURN 1
	END


GO
/****** Object:  Table [dbo].[CHUYENXE]    Script Date: 8/29/2017 12:48:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHUYENXE](
	[MATD] [varchar](50) NOT NULL,
	[STT] [int] NOT NULL,
	[TGBD] [datetime] NULL,
	[TGKT] [datetime] NULL,
 CONSTRAINT [PK_CHUYENXE] PRIMARY KEY CLUSTERED 
(
	[MATD] ASC,
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LICHTRINH]    Script Date: 8/29/2017 12:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LICHTRINH](
	[MAUS] [varchar](50) NOT NULL,
	[MATD] [varchar](50) NOT NULL,
	[STT] [int] NOT NULL,
	[BIENXE] [varchar](50) NULL,
 CONSTRAINT [PK_LICHTRINH] PRIMARY KEY CLUSTERED 
(
	[MAUS] ASC,
	[MATD] ASC,
	[STT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NGUOIDUNG]    Script Date: 8/29/2017 12:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NGUOIDUNG](
	[MAUS] [varchar](50) NOT NULL,
	[TENUS] [nvarchar](50) NULL,
	[TAIKHOAN] [nvarchar](50) NULL,
	[MATKHAU] [nvarchar](50) NULL,
	[DIENTHOAI] [varchar](50) NULL,
	[GIOITINH] [bit] NULL,
	[KHANANG] [float] NULL,
	[MAQUYEN] [varchar](50) NULL,
	[MATO] [varchar](50) NULL,
	[TXQL] [varchar](50) NULL,
 CONSTRAINT [PK_NGUOIDUNG] PRIMARY KEY CLUSTERED 
(
	[MAUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QUYEN]    Script Date: 8/29/2017 12:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QUYEN](
	[MAQUYEN] [varchar](50) NOT NULL,
	[TENQUYEN] [nvarchar](50) NULL,
 CONSTRAINT [PK_QUYEN] PRIMARY KEY CLUSTERED 
(
	[MAQUYEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TOXE]    Script Date: 8/29/2017 12:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TOXE](
	[MATO] [varchar](50) NOT NULL,
	[TENTO] [nvarchar](50) NULL,
	[TOTRUONG] [varchar](50) NULL,
 CONSTRAINT [PK_TO] PRIMARY KEY CLUSTERED 
(
	[MATO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TUYENDUONG]    Script Date: 8/29/2017 12:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TUYENDUONG](
	[MATD] [varchar](50) NOT NULL,
	[TENTD] [nvarchar](50) NULL,
	[CHIEUDAI] [float] NULL,
 CONSTRAINT [PK_TUYENDUONG] PRIMARY KEY CLUSTERED 
(
	[MATD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[NGUOIDUNG] ([MAUS], [TENUS], [TAIKHOAN], [MATKHAU], [DIENTHOAI], [GIOITINH], [KHANANG], [MAQUYEN], [MATO], [TXQL]) VALUES (N'1', N'Nguyễn Quốc Tú', N'tnexnguyen', N'123456', N'01693248887', 1, 500, NULL, NULL, NULL)
INSERT [dbo].[TOXE] ([MATO], [TENTO], [TOTRUONG]) VALUES (N'mato1', N'tocantho', N'1')
INSERT [dbo].[TUYENDUONG] ([MATD], [TENTD], [CHIEUDAI]) VALUES (N'TD1', N'Nguyễn Trãi', 3000)
INSERT [dbo].[TUYENDUONG] ([MATD], [TENTD], [CHIEUDAI]) VALUES (N'TD2', N'Bạch Đằng', 10)
INSERT [dbo].[TUYENDUONG] ([MATD], [TENTD], [CHIEUDAI]) VALUES (N'TD3', N'1235', 12344)
INSERT [dbo].[TUYENDUONG] ([MATD], [TENTD], [CHIEUDAI]) VALUES (N'TD4', N'bc', 3421)
ALTER TABLE [dbo].[CHUYENXE]  WITH CHECK ADD  CONSTRAINT [FK_CHUYENXE_TUYENDUONG] FOREIGN KEY([MATD])
REFERENCES [dbo].[TUYENDUONG] ([MATD])
GO
ALTER TABLE [dbo].[CHUYENXE] CHECK CONSTRAINT [FK_CHUYENXE_TUYENDUONG]
GO
ALTER TABLE [dbo].[LICHTRINH]  WITH CHECK ADD  CONSTRAINT [FK_LICHTRINH_CHUYENXE] FOREIGN KEY([MATD], [STT])
REFERENCES [dbo].[CHUYENXE] ([MATD], [STT])
GO
ALTER TABLE [dbo].[LICHTRINH] CHECK CONSTRAINT [FK_LICHTRINH_CHUYENXE]
GO
ALTER TABLE [dbo].[LICHTRINH]  WITH CHECK ADD  CONSTRAINT [FK_LICHTRINH_NGUOIDUNG] FOREIGN KEY([MAUS])
REFERENCES [dbo].[NGUOIDUNG] ([MAUS])
GO
ALTER TABLE [dbo].[LICHTRINH] CHECK CONSTRAINT [FK_LICHTRINH_NGUOIDUNG]
GO
ALTER TABLE [dbo].[NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_NGUOIDUNG_NGUOIDUNG] FOREIGN KEY([TXQL])
REFERENCES [dbo].[NGUOIDUNG] ([MAUS])
GO
ALTER TABLE [dbo].[NGUOIDUNG] CHECK CONSTRAINT [FK_NGUOIDUNG_NGUOIDUNG]
GO
ALTER TABLE [dbo].[NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_NGUOIDUNG_QUYEN] FOREIGN KEY([MAQUYEN])
REFERENCES [dbo].[QUYEN] ([MAQUYEN])
GO
ALTER TABLE [dbo].[NGUOIDUNG] CHECK CONSTRAINT [FK_NGUOIDUNG_QUYEN]
GO
ALTER TABLE [dbo].[NGUOIDUNG]  WITH CHECK ADD  CONSTRAINT [FK_NGUOIDUNG_TO] FOREIGN KEY([MATO])
REFERENCES [dbo].[TOXE] ([MATO])
GO
ALTER TABLE [dbo].[NGUOIDUNG] CHECK CONSTRAINT [FK_NGUOIDUNG_TO]
GO
ALTER TABLE [dbo].[TOXE]  WITH CHECK ADD  CONSTRAINT [FK_TO_NGUOIDUNG] FOREIGN KEY([TOTRUONG])
REFERENCES [dbo].[NGUOIDUNG] ([MAUS])
GO
ALTER TABLE [dbo].[TOXE] CHECK CONSTRAINT [FK_TO_NGUOIDUNG]
GO
USE [master]
GO
ALTER DATABASE [QLTX] SET  READ_WRITE 
GO
