﻿using DAO;
using DTO;
using System.Data;
namespace BUS
{
  public  class ToaBUS
    {
   
      public static DataTable LayBangToa()
      {
          DataTable bang = new DataTable();
          bang = ToaDAO.LayBangToa();
          return bang;
      }
      public static void ThemToa(ToaDTO toa)
      {
          ToaDAO.Them(toa);
       
      }
      public static void SuaToa(ToaDTO toa)
      {
          ToaDAO.Sua(toa);
        
      }

      public static void XoaToa(ToaDTO toa)
      {
          ToaDAO.Xoa(toa);
        
      }

      
    }
}
