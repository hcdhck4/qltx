﻿using System.Data;
using DAO;
using DTO;

namespace BUS
{
  public  class ChuyenTauBUS
    {
      public static DataTable LayBangChuyenTau()
      {
          DataTable bang = new DataTable();
          bang = ChuyenTauDAO.LayBangChuyenTau();
          return bang;
      }

      public static void ThemChuyenTau(ChuyenTauDTO chuyentau)
      {
          ChuyenTauDAO.Them(chuyentau);
      }

      public static void SuaChuyenTau(ChuyenTauDTO chuyentau)
      {
          ChuyenTauDAO.Sua(chuyentau);
      }

      public static void XoaChuyenTau(ChuyenTauDTO chuyentau)
      {
          ChuyenTauDAO.Xoa(chuyentau);
 
      }



   
    }
}
