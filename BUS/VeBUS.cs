﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DTO;
using DAO;
namespace BUS
{
    class VeBUS
    {

        public static DataTable LayBangVe()
        {
            DataTable bang = new DataTable();
            bang = VeDAO.LayBangVe();
            return bang;
        }

        public static void ThemVe(VeDTO ve)
        {
            VeDAO.ThemVe(ve);
        }

        public static void SuaVe(VeDTO ve)
        {
            VeDAO.SuaVe(ve);
        }

        public static void XoaVe(VeDTO ve)
        {
            VeDAO.XoaVe(ve);
        }
    } 
}
