﻿using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class TuyenBUS
    {
        public static DataTable LayBangTuyen()
        {
            DataTable bang = new DataTable();
            bang = TuyenDAO.LayBangTuyen();
            return bang;
        }
        public static void ThemTuyen(TuyenDTO tuyen)
        {
            TuyenDAO.ThemTuyen(tuyen);

        }

        public static void SuaTuyen(TuyenDTO tuyen)
        {
            TuyenDAO.SuaTuyen(tuyen);

        }

        public static void XoaTuyen(TuyenDTO tuyen)
        {
            TuyenDAO.XoaTuyen(tuyen);

        }

     
    }
}
