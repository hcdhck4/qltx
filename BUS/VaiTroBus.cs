﻿using System.Data;
using DTO;
using DAO;
namespace BUS
{
  public   class VaiTroBus
    {
        public static DataTable LayBangTaiKhoan()
        {
            DataTable bang = new DataTable();
            bang = TaiKhoanDAO.LayBangTaiKhoan();
            return bang;
        }
        public static void ThemTK(TaiKhoanDTO tk)
        {
            TaiKhoanDAO.ThemTK(tk);
        }
        public static void Xoataikhoan(string tk)
        {
            TaiKhoanDAO.Xoataikhoan(tk);
        }
        public static void SuaTK(TaiKhoanDTO tk)
        {
            TaiKhoanDAO.SuaTK(tk);
        }
        public static void DoiMatKhau(int Matk, string matkhau)
        {
            TaiKhoanDAO.DoiMatKhau(Matk, matkhau);
        }
       
    }
}
