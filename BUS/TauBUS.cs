﻿using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class TauBUS
    {
        public static DataTable LayBangTau()
        {
            DataTable bang = new DataTable();
            bang = TauDAO.LayBangTau();
            return bang;
        }
        public static void ThemTau(TauDTO tau)
        {

            TauDAO.Them(tau);
        }
        public static void SuaTau(TauDTO tau)
        {
            TauDAO.Sua(tau);
           
        }
        public static void XoaTau(TauDTO tau)
        {
            TauDAO.Xoa(tau);
        }


    
    }
}
