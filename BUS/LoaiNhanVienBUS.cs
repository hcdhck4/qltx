﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class LoaiNhanVienBUS
    {
        public static DataTable LayLoaiNhanVien()
        {
            DataTable bang = new DataTable();
            bang = LoaiNhanVienDAO.LayLoaiNhanVien();
            return bang;
        }

        public static void ThemLoaiNV(LoaiNhanVienDTO loainv)
        {
            LoaiNhanVienDAO.Them(loainv);
        }

        public static void SuaLoaiNV(LoaiNhanVienDTO loainv)
        {
            LoaiNhanVienDAO.Sua(loainv);
        }

        public static void XoaLoaiNV(LoaiNhanVienDTO loainv)
        {
            LoaiNhanVienDAO.Xoa(loainv);
        }
    }
}
