﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class DonViTinhBUS
    {
        public static DataTable LayBangDVTinh()
        {
            DataTable bang = new DataTable();
            bang = DonViTinhDAO.LayBangDVTinh();
            return bang;
        }

        public static void ThemDVT(DonViTinhDTO dvt)
        {
            DonViTinhDAO.Them(dvt);
            
        }

        public static void SuaDVT(DonViTinhDTO dvt)
        {
            DonViTinhDAO.Sua(dvt);
        }

        public static void XoaDVT(DonViTinhDTO dvt)
        {
            DonViTinhDAO.Xoa(dvt);
        }


        
    }
}
