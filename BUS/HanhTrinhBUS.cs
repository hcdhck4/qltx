﻿using System.Data;
using DAO;
using DTO;
namespace BUS
{
    public class HanhTrinhBUS
    {
        public static DataTable LayBangHanhTrinh()
        {
            DataTable bang = new DataTable();
            bang = HanhTrinhDAO.LayBangHanhTrinh();
            return bang;
        }

        public static void ThemHanhTrinh(HanhTrinhDTO hanhtrinh)
        {
            HanhTrinhDAO.Them(hanhtrinh);
        }
        public static void SuaHanhTrinh(HanhTrinhDTO hanhtrinh)
        {
            HanhTrinhDAO.Sua(hanhtrinh);
        }
        public static void XoaHanhTrinh(HanhTrinhDTO hanhtrinh)
        {
            HanhTrinhDAO.Xoa(hanhtrinh);
        }


    
    }
}
