﻿using System.Data;
using DAO;
using DTO;

namespace BUS
{
    public class HanhKhachBUS
    {
        public static DataTable LayBangKhachHang()
        {
            DataTable bang = new DataTable();
            bang = HanhKhachDAO.LayBangKhachHang();
            return bang;
        }

        public static void ThemHK(HanhKhachDTO hk)
        {
            HanhKhachDAO.Them(hk);           
        }

        public static void XoaHK(HanhKhachDTO hk)
        {
            HanhKhachDAO dao = new HanhKhachDAO();
            dao.Xoa(hk);
        }

        public static void SuaHK(HanhKhachDTO hk)
        {
            HanhKhachDAO dao = new HanhKhachDAO();
            dao.Sua(hk);
        }



        
    }
}
