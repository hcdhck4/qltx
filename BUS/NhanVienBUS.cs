﻿using DAO;
using System.Data;
using DTO;

namespace BUS
{
    public class NhanVienBUS
    {
        public static DataTable LayBangNhanvien()
        {
            DataTable bang = new DataTable();
            bang = NhanVienDAO.LayBangNhanvien();
            return bang;
        }

        public static void ThemNV(NhanVienDTO nv)
        {
            NhanVienDAO.Them(nv);
        }

        public static void XoaNV(NhanVienDTO nv)
        {
            NhanVienDAO.Xoa(nv);

        }

        public static void SuaNV(NhanVienDTO nv)
        {
            NhanVienDAO.Sua(nv);
        }
        public static DataTable LayTenMaNhanvien()
        {
            DataTable bang = new DataTable();
            bang = NhanVienDAO.LayTenMaNhanvien();
            return bang;
        }
 
    }
}
