﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAO;
using DTO;

namespace BUS
{
   public class GaBUS
    {
        public static DataTable LayGa()
        {
            DataTable bang = new DataTable();
            bang = GaDAO.LayGa();
            return bang;
        }

        public static void ThemGa(GaDTO ga)
        {
            GaDAO.Them(ga);
        }

        public static void SuaGa(GaDTO ga)
        {
            GaDAO.Sua(ga);
           
        }

        public static void XoaGa(GaDTO ga)
        {
            GaDAO.Xoa(ga);
         
        }

        public static DataTable LayGaMa_Ten()
        {
            DataTable bang = new DataTable();
            bang = GaDAO.LayGaMa_Ten();
            return bang;

        }

 
    }
}
