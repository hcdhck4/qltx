﻿using System;

namespace DTO
{
    public class LoaiNhanVienDTO
    {
        private string _maloainv;

        public string Maloainv
        {
            get { return _maloainv; }
            set { _maloainv = value; }
        }

        private string _tenvt;

        public string Tenvt
        {
            get { return _tenvt; }
            set { _tenvt = value; }
        }

        private string _ghichu;

        public string Ghichu
        {
            get { return _ghichu; }
            set { _ghichu = value; }
        }
    }
}
