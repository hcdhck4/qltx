﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class Tau_ToaDTOcs
    {
        private String matau;

        public String MaTau
        {
            get { return matau; }
            set { matau = value; }
        }
        private String loaitau;

        public String LoaiTau
        {
            get { return loaitau; }
            set { loaitau = value; }
        }
        private String loaitoa;

        public String LoaiToa
        {
            get { return loaitoa; }
            set { loaitoa = value; }
        }
        private DateTime Ngay;

        public DateTime ngay
        {
            get { return ngay; }
            set { ngay = value; }

        }
        private string gadi;

        public string GaDi
        {
            get { return gadi; }
            set { gadi = value; }
        }
        private string gaden;

        public string GaDen
        {
            get { return gaden; }
            set { gaden = value; }
        }
    }
}
