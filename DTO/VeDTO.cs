﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class VeDTO
    {
        private string _mave;

        public string Mave
        {
            get { return _mave; }
            set { _mave = value; }
        }

        private string _tinhtrangve;

        public string Tinhtrangve
        {
            get { return _tinhtrangve; }
            set { _tinhtrangve = value; }
        }

        private string _chuyentau;

        public string Chuyentau
        {
            get { return _chuyentau; }
            set { _chuyentau = value; }
        }

        private string _ghe;

        public string Ghe
        {
            get { return _ghe; }
            set { _ghe = value; }
        }

        private string _giuong;

        public string Giuong
        {
            get { return _giuong; }
            set { _giuong = value; }
        }

        private DateTime _ngaytao;

        public DateTime Ngaytao
        {
            get { return _ngaytao; }
            set { _ngaytao = value; }
        }

        private string _nhanvientao;

        public string Nhanvientao
        {
            get { return _nhanvientao; }
            set { _nhanvientao = value; }
        }

        private string _gia;

        public string Gia
        {
            get { return _gia; }
            set { _gia = value; }
        }

        private string _doituong;

        public string Doituong
        {
            get { return _doituong; }
            set { _doituong = value; }
        }
    }
}
