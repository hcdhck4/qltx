﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class ChuyenTauDTO
    {


        private DateTime _thoigiankhoihanh;

        public DateTime Thoigiankhoihanh
        {
            get { return _thoigiankhoihanh; }
            set { _thoigiankhoihanh = value; }
        }

        private DateTime _thoigianden;

        public DateTime Thoigianden
        {
            get { return _thoigianden; }
            set { _thoigianden = value; }
        }



        private string _matuyen;

        public string Matuyen
        {
            get { return _matuyen; }
            set { _matuyen = value; }
        }


        private int _stt;

        public int STT
        {
            get { return _stt; }
            set { _stt = value; }
        }
    }
}
