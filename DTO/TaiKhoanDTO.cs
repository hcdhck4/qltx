﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public  class TaiKhoanDTO
    {
        private string _matk;

        public string Matk
        {
            get { return _matk; }
            set { _matk = value; }
        }
        private string _tentk;

        public string Tentk
        {
            get { return _tentk; }
            set { _tentk = value; }
        }
        private string _matkhau;

        public string Matkhau
        {
            get { return _matkhau; }
            set { _matkhau = value; }
        }
        private Boolean _tinhtrang;

        public Boolean Tinhtrang
        {
            get { return _tinhtrang; }
            set { _tinhtrang = value; }
        }
        private string _ghichu;

        public string Ghichu
        {
            get { return _ghichu; }
            set { _ghichu = value; }
        }
        private string _manv;

        public string Manv
        {
            get { return _manv; }
            set { _manv = value; }
        }
        private string _vaitro;

        public string Vaitro
        {
            get { return _vaitro; }
            set { _vaitro = value; }
        }
        private string _makhang;

        public string Makhang
        {
            get { return _makhang; }
            set { _makhang = value; }
        }
    }
}
