﻿
namespace DTO
{
    public class TuyenDTO
    {
        private string _matuyen;

        public string Matuyen
        {
            get { return _matuyen; }
            set { _matuyen = value; }
        }

        private string _tentuyen;

        public string Tentuyen
        {
            get { return _tentuyen; }
            set { _tentuyen = value; }
        }

        private string _chieudai;

        public string Chieudai
        {
            get { return _chieudai; }
            set { _chieudai = value; }
        }
    }
}
