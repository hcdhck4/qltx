﻿
namespace DTO
{
    public class DonViTinhDTO
    {
        private string _madv;

        public string Madv
        {
            get { return _madv; }
            set { _madv = value; }
        }

        private string _ten;

        public string Ten
        {
            get { return _ten; }
            set { _ten = value; }
        }

        private string _ghichu;

        public string Ghichu
        {
            get { return _ghichu; }
            set { _ghichu = value; }
        }
    }

}
