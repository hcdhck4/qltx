﻿using System;
namespace DTO
{
    public class HanhTrinhDTO
    {
        private string _mahanhtrinh;

        public string Mahanhtrinh
        {
            get { return _mahanhtrinh; }
            set { _mahanhtrinh = value; }
        }

        private DateTime _thoidiemxuatphat;

        public DateTime Thoidiemxuatphat
        {
            get { return _thoidiemxuatphat; }
            set { _thoidiemxuatphat = value; }
        }

        private float _khoangKm;

        public float KhoangKm
        {
            get { return _khoangKm; }
            set { _khoangKm = value; }
        }

        private string _matuyen;

        public string Matuyen
        {
            get { return _matuyen; }
            set { _matuyen = value; }
        }

        private string _maga;

        public string Maga
        {
            get { return _maga; }
            set { _maga = value; }
        }

        private string _donvitinh;

        public string Donvitinh
        {
            get { return _donvitinh; }
            set { _donvitinh = value; }
        }
    }
}
