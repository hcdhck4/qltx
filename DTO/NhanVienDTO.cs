﻿using System;

namespace DTO
{
    public class NhanVienDTO
    {
        private string _manv;

        public string Manv
        {
            get { return _manv; }
            set { _manv = value; }
        }
        private string _tennv;

        public string Tennv
        {
            get { return _tennv; }
            set { _tennv = value; }
        }
        private string _cmnd;

        public string Cmnd
        {
            get { return _cmnd; }
            set { _cmnd = value; }
        }
        private DateTime _ngaysinh;

        public DateTime Ngaysinh
        {
            get { return _ngaysinh; }
            set { _ngaysinh = value; }
        }
        private bool _gioitinh;

        public bool Gioitinh
        {
            get { return _gioitinh; }
            set { _gioitinh = value; }
        }

        private string _diachi;

        public string Diachi
        {
            get { return _diachi; }
            set { _diachi = value; }
        }

        private string _dienthoai;

        public string Dienthoai
        {
            get { return _dienthoai; }
            set { _dienthoai = value; }
        }

        private string _loai;

        public string Loai
        {
            get { return _loai; }
            set { _loai = value; }
        }

        private string _nvquanly;

        public string Nvquanly
        {
            get { return _nvquanly; }
            set { _nvquanly = value; }
        }

        private string _maga;

        public string Maga
        {
            get { return _maga; }
            set { _maga = value; }
        }
    }
}
