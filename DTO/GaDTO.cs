﻿
namespace DTO
{
    public class GaDTO
    {
        private string _maga;

        public string Maga
        {
            get { return _maga; }
            set { _maga = value; }
        }

        private string _tenga;

        public string Tenga
        {
            get { return _tenga; }
            set { _tenga = value; }
        }

        private string _ghichu;

        public string Ghichu
        {
            get { return _ghichu; }
            set { _ghichu = value; }
        }
    }
}
