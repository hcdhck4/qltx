﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
   public class TauDTO
    {
        private string _matau;

        public string Matau
        {
            get { return _matau; }
            set { _matau = value; }
        }

        private string _tentau;

        public string Tentau
        {
            get { return _tentau; }
            set { _tentau = value; }
        }

        private string _hangSX;

        public string HangSX
        {
            get { return _hangSX; }
            set { _hangSX = value; }
        }

        private DateTime _ngayvanhang;

        public DateTime Ngayvanhang
        {
            get { return _ngayvanhang; }
            set { _ngayvanhang = value; }
        }

        private string _loaitau;

        public string Loaitau
        {
            get { return _loaitau; }
            set { _loaitau = value; }
        }

        private int _ikm;

        public int Ikm
        {
            get { return _ikm; }
            set { _ikm = value; }
        }

        private int _ktautoida;

        public int Ktautoida
        {
            get { return _ktautoida; }
            set { _ktautoida = value; }
        }

        private int _ztautoithieu;

        public int Ztautoithieu
        {
            get { return _ztautoithieu; }
            set { _ztautoithieu = value; }
        }
    }
}
