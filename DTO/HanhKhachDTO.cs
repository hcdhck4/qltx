﻿using System;
namespace DTO
{
    public  class HanhKhachDTO
    {
        private string _makhach;

        public string Makhach
        {
            get { return _makhach; }
            set { _makhach = value; }
        }

        private string _hoten;

        public string Hoten
        {
            get { return _hoten; }
            set { _hoten = value; }
        }

        private string _cmnd;

        public string Cmnd
        {
            get { return _cmnd; }
            set { _cmnd = value; }
        }

        private DateTime _ngaysinh;

        public DateTime Ngaysinh
        {
            get { return _ngaysinh; }
            set { _ngaysinh = value; }
        }

        private string  _gioitinh;

        public string Gioitinh
        {
            get { return _gioitinh; }
            set { _gioitinh = value; }
        }

        private string _diachi;

        public string Diachi
        {
            get { return _diachi; }
            set { _diachi = value; }
        }

        private string _dienthoai;

        public string Dienthoai
        {
            get { return _dienthoai; }
            set { _dienthoai = value; }
        }

        private string _loaiKH;

        public string LoaiKH
        {
            get { return _loaiKH; }
            set { _loaiKH = value; }
        }

        private string _doituong;

        public string Doituong
        {
            get { return _doituong; }
            set { _doituong = value; }
        }
     

    }
}
