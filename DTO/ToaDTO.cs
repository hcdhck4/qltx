﻿
namespace DTO
{
    public class ToaDTO
    {
        private string _matoa;

        public string Matoa
        {
            get { return _matoa; }
            set { _matoa = value; }
        }

        private string _sohieu;

        public string Sohieu
        {
            get { return _sohieu; }
            set { _sohieu = value; }
        }

        private string _loaitoa;

        public string Loaitoa
        {
            get { return _loaitoa; }
            set { _loaitoa = value; }
        }

        private string _matau;

        public string Matau
        {
            get { return _matau; }
            set { _matau = value; }
        }

    }
}
