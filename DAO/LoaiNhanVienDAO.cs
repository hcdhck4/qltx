﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DTO;

namespace DAO
{
    public class LoaiNhanVienDAO
    {
        public static DataTable LayLoaiNhanVien()
        {
            DataTable bang = new DataTable();
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYlOAINHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            return bang;
        }
        public static void Them(LoaiNhanVienDTO loainv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "THEM_LOAINHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Maloainv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Maloainv ;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@tenvt", SqlDbType.NVarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Tenvt;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@ghichu", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Ghichu;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void Sua(LoaiNhanVienDTO loainv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SUA_LOAINHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Maloainv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Maloainv;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@tenvt", SqlDbType.NVarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Tenvt;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@ghichu", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Ghichu;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void Xoa(LoaiNhanVienDTO loainv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "XOA_LOAINHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Maloainv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = loainv.Maloainv;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
