﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
  public  class DonViTinhDAO
    {
      public static DataTable LayBangDVTinh()
      {
          DataTable bang = new DataTable();

          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "LAYDVTINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlDataAdapter Adt = new SqlDataAdapter(cmd);
          Adt.Fill(bang);

          cn.Close();
          return bang;
      }

      public static void Them(DonViTinhDTO dvt)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "THEM_DONVITINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Madv", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Madv;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Ten", SqlDbType.NVarChar, 30);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Ten ;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Ghichu", SqlDbType.NVarChar,200);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Ghichu;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      public static void Sua(DonViTinhDTO dvt)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "SUA_DONVITINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Madv", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Madv;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Ten", SqlDbType.NVarChar, 30);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Ten;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Ghichu", SqlDbType.NVarChar, 200);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Ghichu;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      public static void Xoa(DonViTinhDTO dvt)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "XOA_DONVITINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Madv", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = dvt.Madv;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      
    }
}
