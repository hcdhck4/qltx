﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
    public class VeDAO
    {
        public static DataTable LayBangVe()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYVE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }

        public static void ThemVe(VeDTO ve)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "THEM_VE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Mave", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Mave;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tinhtrangve", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Tinhtrangve;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Chuyen", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Chuyentau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ghe", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Ghe;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Giuong", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Giuong;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Ngaytao", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Ngaytao;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Nhanvientao", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Nhanvientao;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Giave ", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Gia;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@DoiTuong", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Doituong;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void SuaVe(VeDTO ve)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SUA_VE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Mave", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Mave;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tinhtrangve", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Tinhtrangve;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Chuyen", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Chuyentau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ghe", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Ghe;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Giuong", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Giuong;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Ngaytao", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Ngaytao;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Nhanvientao", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Nhanvientao;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@Giave ", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Gia;
            cmd.Parameters.Add(para);


            para = new SqlParameter("@DoiTuong", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Doituong;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }


        public static void XoaVe(VeDTO ve)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "XOA_VE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Mave", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = ve.Mave;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

    }
}
