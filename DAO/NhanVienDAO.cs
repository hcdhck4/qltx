﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
  public class NhanVienDAO
    {
        public static DataTable LayBangNhanvien()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYNHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }
        public static DataTable LayTenMaNhanvien()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYNHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }

        public static void Them(NhanVienDTO nv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "THEM_NHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Manv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Manv;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tennv", SqlDbType.NVarChar , 50);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Tennv;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Cmnd ", SqlDbType.NVarChar ,20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Cmnd;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ngaysinh", SqlDbType.DateTime);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Ngaysinh;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@GioiTinh", SqlDbType.NVarChar, 3);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Gioitinh;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Diachi", SqlDbType.NVarChar,30);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Diachi;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@DienThoai", SqlDbType.NVarChar , 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Dienthoai;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Loainv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Loai;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@NVquanly", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Nvquanly;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Maga;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void Sua(NhanVienDTO nv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SUA_NHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Manv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Manv;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tennv", SqlDbType.NVarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Tennv;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Cmnd ", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Cmnd;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ngaysinh", SqlDbType.DateTime);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Ngaysinh;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@GioiTinh", SqlDbType.NVarChar, 3);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Gioitinh;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Diachi", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Diachi;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@DienThoai", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Dienthoai;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Loainv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Loai;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@NVquanly", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Nvquanly;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Maga;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void Xoa(NhanVienDTO nv)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "XOA_NHANVIEN";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("Manv", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = nv.Manv;
            cmd.Parameters.Add(para);


            cmd.ExecuteNonQuery();
            cn.Close();

        }


    }
}
