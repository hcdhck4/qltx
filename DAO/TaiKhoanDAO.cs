﻿using System.Data;
using System.Data.SqlClient;
using DTO;
using System;
namespace DAO
{
  public class TaiKhoanDAO
    {
      public static DataTable LayBangTaiKhoan()
      {
          DataTable bang = new DataTable();

          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "laytaikhoan";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlDataAdapter Adt = new SqlDataAdapter(cmd);
          Adt.Fill(bang);

          cn.Close();
          return bang;
      }

      public static void ThemTK(TaiKhoanDTO tk)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "themtaikhoan";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;


          cmd.Parameters.Add("@tennguoidung", SqlDbType.VarChar, 50).Value = tk.Tentk;
          cmd.Parameters.Add("@matkhau", SqlDbType.VarChar, 50).Value =tk.Matkhau;
          cmd.Parameters.Add("@tinhtrang", SqlDbType.Bit).Value = tk.Tinhtrang;
          cmd.Parameters.Add("@ghichu", SqlDbType.NVarChar,30).Value= tk.Ghichu;

          cmd.Parameters.Add("@vaitro", SqlDbType.NChar, 20).Value = tk.Vaitro;
          if (tk.Manv != null)
          {
              cmd.Parameters.Add("@manv", SqlDbType.VarChar, 20).Value = tk.Manv;
          }
          else { cmd.Parameters.Add("@manv", SqlDbType.VarChar, 20).Value = DBNull.Value; }
          if (tk.Makhang != null)
          {
              cmd.Parameters.Add("@makh", SqlDbType.VarChar, 20).Value = tk.Makhang;
          }
          else { cmd.Parameters.Add("@makh", SqlDbType.VarChar, 20).Value = DBNull.Value;}
               
          cmd.ExecuteNonQuery();
          cn.Close();

      }
      public static void Xoataikhoan(string tk)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "Xoataikhoan";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();


          para = new SqlParameter("@matk", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = tk;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }
      public static void SuaTK(TaiKhoanDTO tk)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "suataikhoan";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          cmd.Parameters.Add("@matk", SqlDbType.Int ).Value = tk.Matk;
          cmd.Parameters.Add("@tennguoidung", SqlDbType.VarChar, 50).Value = tk.Tentk;
          cmd.Parameters.Add("@matkhau", SqlDbType.VarChar, 50).Value = tk.Matkhau;
          cmd.Parameters.Add("@tinhtrang", SqlDbType.Bit).Value = tk.Tinhtrang;
          cmd.Parameters.Add("@ghichu", SqlDbType.NVarChar, 30).Value = tk.Ghichu;

          cmd.Parameters.Add("@vaitro", SqlDbType.NChar, 20).Value = tk.Vaitro;
          if (tk.Manv != null)
          {
              cmd.Parameters.Add("@manv", SqlDbType.VarChar, 20).Value = tk.Manv;
          }
          else { cmd.Parameters.Add("@manv", SqlDbType.VarChar, 20).Value = DBNull.Value; }
          if (tk.Makhang != null)
          {
              cmd.Parameters.Add("@makh", SqlDbType.VarChar, 20).Value = tk.Makhang;
          }
          else { cmd.Parameters.Add("@makh", SqlDbType.VarChar, 20).Value = DBNull.Value; }

          cmd.ExecuteNonQuery();
          cn.Close();

      }
      public static void DoiMatKhau(int  Matk,string matkhau)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "thaydoimatkhau";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@matk", SqlDbType.Int);
          para.Direction = ParameterDirection.Input;
          para.Value = Matk;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@matkhau", SqlDbType.VarChar, 50);
          para.Direction = ParameterDirection.Input;
          para.Value = matkhau;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }
     
    }
}
