﻿using System.Data;
using System.Data.SqlClient;
using DTO;


namespace DAO
{
    public class TauDAO
    {

        public static DataTable LayBangTau()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYTAU";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }

        public static void Them(TauDTO tau)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "THEM_TAU";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Matau ", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Matau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tentau",SqlDbType .NVarChar ,30);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Tentau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@HangSX", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.HangSX;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ngayvanhanh", SqlDbType.Date);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ngayvanhang;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Loaitau", SqlDbType.NVarChar ,20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Loaitau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ikm", SqlDbType.Int );
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ikm ;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ktoatoida", SqlDbType.Int);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ktautoida;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ztoatoithieu", SqlDbType.Int);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ztautoithieu;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();

        }


        public static void Sua(TauDTO tau)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SUA_TAU";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@Matau ", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Matau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Tentau", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Tentau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@HangSX", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.HangSX;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ngayvanhanh", SqlDbType.Date);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ngayvanhang;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Loaitau", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Loaitau;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ikm", SqlDbType.Int);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ikm;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ktoatoida", SqlDbType.Int);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ktautoida;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@Ztoatoithieu", SqlDbType.Int);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Ztautoithieu;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();

        }

        public static void Xoa(TauDTO tau)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "XOA_TAU";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();
            para = new SqlParameter("@Matau",SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = tau.Matau;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
