﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
 public   class TuyenDAO
    {

        public static DataTable LayBangTuyen()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_GetTuyenDuong";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }

        public static void ThemTuyen(TuyenDTO tuyen)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_CreateTuyenDuong";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@maTD", SqlDbType.VarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Matuyen;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@tenTD", SqlDbType.NVarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Tentuyen;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@chieuDai", SqlDbType.Float);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Chieudai;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void SuaTuyen(TuyenDTO tuyen)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_UpdateTuyenDuong";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@maTD", SqlDbType.VarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Matuyen;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@tenTD", SqlDbType.NVarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Tentuyen;
            cmd.Parameters.Add(para);

            para = new SqlParameter("@chieuDai", SqlDbType.Float);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Chieudai;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void XoaTuyen(TuyenDTO tuyen)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_DeleteTuyenDuong";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();

            para = new SqlParameter("@maTD", SqlDbType.VarChar, 50);
            para.Direction = ParameterDirection.Input;
            para.Value = tuyen.Matuyen;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

    }
}
