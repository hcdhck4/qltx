﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
   public class GaDAO
    {
       public static DataTable LayGa()
       {
           DataTable bang = new DataTable();

           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "LAYGA";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlDataAdapter Adt = new SqlDataAdapter(cmd);
           Adt.Fill(bang);

           cn.Close();
           return bang;
       }

       public static void Them(GaDTO ga)
       {
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "THEM_GA";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();

           para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Maga;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@Tenga", SqlDbType.NVarChar, 50);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Tenga;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@ghichu", SqlDbType.NVarChar, 200);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Ghichu ;
           cmd.Parameters.Add(para);

           cmd.ExecuteNonQuery();
           cn.Close();

       }

       public static void Sua(GaDTO ga)
    
       {
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "@SUA_TOA";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();

           para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Maga;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@Tenga", SqlDbType.NVarChar, 50);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Tenga;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@ghichu", SqlDbType.NVarChar, 200);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Ghichu;
           cmd.Parameters.Add(para);

           cmd.ExecuteNonQuery();
           cn.Close();

       }
       public static void Xoa(GaDTO ga)
       {
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "XOA_TOA";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();

           para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
           para.Direction = ParameterDirection.Input;
           para.Value = ga.Maga;
           cmd.Parameters.Add(para);

           cmd.ExecuteNonQuery();
           cn.Close();
       }
       public static DataTable LayGaMa_Ten()
       {
           DataTable bang = new DataTable();

           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "LAYGAMA_TEN";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlDataAdapter Adt = new SqlDataAdapter(cmd);
           Adt.Fill(bang);

           cn.Close();
           return bang;
       }

    }
}
