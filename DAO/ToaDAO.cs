﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
  public class ToaDAO
    {

      public static DataTable LayBangToa()
      {
          DataTable bang = new DataTable();

          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "LAYTOA";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlDataAdapter Adt = new SqlDataAdapter(cmd);
          Adt.Fill(bang);

          cn.Close();
          return bang;
      }

      public static void Them(ToaDTO toa)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "THEM_TOA";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;


          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Matoa", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Matoa;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Sohieu", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Sohieu;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Loaitoa", SqlDbType.NVarChar , 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Loaitoa;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Matau", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Matau;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      public static void Sua(ToaDTO toa)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "SUA_TOA";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;


          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Matoa", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Matoa;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Sohieu", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Sohieu;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Loaitoa", SqlDbType.NVarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Loaitoa;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Matau", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Matau;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      public static void Xoa(ToaDTO toa)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "XOA_TOA";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;


          SqlParameter para = new SqlParameter();

          para = new SqlParameter("@Matoa", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = toa.Matoa;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }
    }
}
