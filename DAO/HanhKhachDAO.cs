﻿using System.Data;
using System.Data.SqlClient;
using DTO;
using System;
namespace DAO
{
   public class HanhKhachDAO
    {
        public static DataTable LayBangKhachHang()
        {
            DataTable bang = new DataTable();

            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LAYKHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlDataAdapter Adt = new SqlDataAdapter(cmd);
            Adt.Fill(bang);

            cn.Close();
            return bang;
        }

        public  static void Them(HanhKhachDTO hk)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "THEM_KHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();


            para = new SqlParameter("Makhach", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Makhach;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Hoten", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Hoten;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Cmnd", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Cmnd;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Ngaysinh", SqlDbType.DateTime, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Ngaysinh;
            cmd.Parameters.Add(para);


            para = new SqlParameter("GioiTinh", SqlDbType.NVarChar, 3);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Gioitinh;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Diachi", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Diachi;
            cmd.Parameters.Add(para);


            para = new SqlParameter("DienThoai", SqlDbType.NVarChar , 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Dienthoai;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Loaikh", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.LoaiKH;
            cmd.Parameters.Add(para);

            if (hk.Doituong == null)
            {
                para = new SqlParameter("DoiTuong", SqlDbType.VarChar, 20);
                para.Direction = ParameterDirection.Input;
                para.Value = DBNull.Value;
                cmd.Parameters.Add(para);
            }
            else
            {
                para = new SqlParameter("DoiTuong", SqlDbType.VarChar, 20);
                para.Direction = ParameterDirection.Input;
                para.Value = hk.Doituong;
                cmd.Parameters.Add(para);
            }
            

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void Sua(HanhKhachDTO hk)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SUA_KHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();


            para = new SqlParameter("Makhach", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Makhach;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Hoten", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Hoten;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Cmnd", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Cmnd;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Ngaysinh", SqlDbType.DateTime, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Ngaysinh;
            cmd.Parameters.Add(para);


            para = new SqlParameter("GioiTinh", SqlDbType.NVarChar, 3);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Gioitinh;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Diachi", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Diachi;
            cmd.Parameters.Add(para);


            para = new SqlParameter("DienThoai", SqlDbType.NVarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Dienthoai;
            cmd.Parameters.Add(para);


            para = new SqlParameter("Loaikh", SqlDbType.NVarChar, 30);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.LoaiKH;
            cmd.Parameters.Add(para);


            para = new SqlParameter("DoiTuong", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Doituong;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void Xoa(HanhKhachDTO hk)
        {
            SqlConnection cn;
            cn = DataProvider.Connection();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "XOA_KHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            SqlParameter para = new SqlParameter();


            para = new SqlParameter("Makhach", SqlDbType.VarChar, 20);
            para.Direction = ParameterDirection.Input;
            para.Value = hk.Makhach;
            cmd.Parameters.Add(para);

            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
