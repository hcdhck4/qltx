﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
   public class ChuyenTauDAO
    {
       public static DataTable LayBangChuyenTau()
       {
           DataTable bang = new DataTable();

           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "sp_GetChuyenXe";
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Connection = cn;

           SqlDataAdapter Adt = new SqlDataAdapter(cmd);
           Adt.Fill(bang);

           cn.Close();
           return bang;
       }

       public static void Them(ChuyenTauDTO chuyentau)
       {
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "sp_CreateChuyenXe";
           cmd.CommandType = CommandType.StoredProcedure;

           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();
           para = new SqlParameter("@Stt", SqlDbType.Int);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.STT;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@Matd", SqlDbType.VarChar , 50);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Matuyen;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@TGBD", SqlDbType.DateTime);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Thoigiankhoihanh;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@TGKT", SqlDbType.DateTime);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Thoigianden;
           cmd.Parameters.Add(para);

           cmd.ExecuteNonQuery();
           cn.Close();
       }

       public static void Sua(ChuyenTauDTO chuyentau)
       { 
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "sp_UpdateChuyenXe";
           cmd.CommandType = CommandType.StoredProcedure;

           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();
           para = new SqlParameter("@Stt", SqlDbType.Int);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.STT;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@Matd", SqlDbType.VarChar, 50);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Matuyen;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@TGBD", SqlDbType.DateTime);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Thoigiankhoihanh;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@TGKT", SqlDbType.DateTime);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Thoigianden;
           cmd.Parameters.Add(para);

           cmd.ExecuteNonQuery();
           cn.Close();
       }

       public static void Xoa(ChuyenTauDTO chuyentau)
       {
           SqlConnection cn;
           cn = DataProvider.Connection();

           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "sp_DeleteChuyenXe";
           cmd.CommandType = CommandType.StoredProcedure;

           cmd.Connection = cn;

           SqlParameter para = new SqlParameter();
           para = new SqlParameter("@maTD", SqlDbType.VarChar, 50);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.Matuyen;
           cmd.Parameters.Add(para);

           para = new SqlParameter("@sTT", SqlDbType.Int);
           para.Direction = ParameterDirection.Input;
           para.Value = chuyentau.STT;
           cmd.Parameters.Add(para);
           cmd.ExecuteNonQuery();
           cn.Close();
       }
    
    }
}
