﻿using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAO
{
  public class HanhTrinhDAO
    {
      public static DataTable LayBangHanhTrinh()
      {
          DataTable bang = new DataTable();

          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "LAYHANHTRINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlDataAdapter Adt = new SqlDataAdapter(cmd);
          Adt.Fill(bang);

          cn.Close();
          return bang;
      }

      public static void Them(HanhTrinhDTO hanhtrinh)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "THEM_HANHTRINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();
          para = new SqlParameter("@Mahanhtrinh", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Mahanhtrinh;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Thoidiemxuatphat",SqlDbType .DateTime );
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Thoidiemxuatphat ;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@KhoangKm", SqlDbType.Float);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.KhoangKm ;
          cmd.Parameters.Add(para);


          para = new SqlParameter("@Matuyen", SqlDbType.VarChar ,20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Matuyen;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Maga;
          cmd.Parameters.Add(para);


          para = new SqlParameter("@Donvitinh", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Donvitinh;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }

      public static void Sua(HanhTrinhDTO hanhtrinh)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "SUA_HANHTRINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();
          para = new SqlParameter("@Mahanhtrinh", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Mahanhtrinh;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Thoidiemxuatphat", SqlDbType.DateTime);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Thoidiemxuatphat;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@KhoangKm", SqlDbType.Float);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.KhoangKm;
          cmd.Parameters.Add(para);


          para = new SqlParameter("@Matuyen", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Matuyen;
          cmd.Parameters.Add(para);

          para = new SqlParameter("@Maga", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Maga;
          cmd.Parameters.Add(para);


          para = new SqlParameter("@Donvitinh", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Donvitinh;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }


      public static void Xoa(HanhTrinhDTO hanhtrinh)
      {
          SqlConnection cn;
          cn = DataProvider.Connection();

          SqlCommand cmd = new SqlCommand();
          cmd.CommandText = "XOA_HANHTRINH";
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;

          SqlParameter para = new SqlParameter();
          para = new SqlParameter("@Mahanhtrinh", SqlDbType.VarChar, 20);
          para.Direction = ParameterDirection.Input;
          para.Value = hanhtrinh.Mahanhtrinh;
          cmd.Parameters.Add(para);

          cmd.ExecuteNonQuery();
          cn.Close();
      }
    }
}
